# GNB is Not a Bank / GNB No es un Banco

GNB es un software para cooperativas integrales/asociaciones para poder llevar sus finanzas internas

El nombre de GNB significa en inglés (G)NB-is (N)ot a (B)ank o en castellano (G)NB (N)o es un (B)anco... unas siglas recursivas homenaje al movimiento de software libre.

Explicitamos que no es una banco ya que con GNB no se crea dinero de la nada como así lo hacen los bancos comerciales con su privilegio exclusivo (¿Es pues un oxímoron la Banca Ética?). 

Sirve más bien como software/plataforma para llevar las cuentas de los miembros de la cooperativa o asociación y faciliar que la gente pueda ver sus saldos internos, hacer transferencias internas/externas, crear facturas, añadir hojas de gastos, etc.

Para que GNB sea robusto, usamos doble contabilidad, actualmente se apoya con ODOO (un sistema ERP libre que usan muchas cooperativas).

GNB está pensando para federarse con otras instancias de GNB y así crear un ecosistema donde entidades afines(que pueden incluso estar en paises distintos y usar monedas distintas) pueden hacer intercambios de forma segura y no trazable. 

Para estar más informado, puedes seguir nuestros progresos en el [foro](https://foro.komun.org/c/gnb/21) de Komun.org
