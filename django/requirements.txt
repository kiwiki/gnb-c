Django==4.2.6
django-grappelli==3.0.8
psycopg2-binary==2.9.9
psycopg2==2.9.9 #Comment this only if your instance does not uses Postgresql. May require installing libpq-dev or postgresql-common
django-baton==2.8.1
requests==2.31.0
Pillow==10.1.0
django-admin-sortable2==2.1.10
pdfkit==1.0.0
#django-bulma
django-cors-headers==4.3.0
django-notifications-hq==1.8.3
#Download manually odoo-client @ git+https://github.com/jam-odoo/odoo-client-api.git
xlrd==2.0.1
colorama==0.4.6
sentry-sdk==1.39.2 #Only if you use sentry for reporting errors.
django-debug-toolbar==4.3.0
django-model-utils==4.3.1
django-registration==3.4
django-registration-defaults @ git+https://github.com/dfrankow/django-registration-defaults.git@5132f823c619f23a77386ffce3f4ce303a2d016d
