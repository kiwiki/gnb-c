from django.conf import settings
from odooclient import client

odoo_server = client.OdooClient(
    host="gestion.estraperlo.org",
    port=443,
    dbname=settings.ODOO_DBNAME,
    saas=True,
    debug=hasattr(settings, "DEBUG_ODOO"),
)
if not odoo_server.IsAuthenticated():
    print("Odoo connection", odoo_server.ServerInfo())
    odoo_server.Authenticate(settings.ODOO_USERNAME, settings.ODOO_PASSWORD)
    print("Odoo DBNAME", settings.ODOO_DBNAME)
