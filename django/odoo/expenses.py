from colorama import Fore, Style, init
from . import odoo_server
import gnb.utils as utils
from odoo.views import add_odoo_atachment_to_resource
import odoo.accounts
from django.conf import settings

init()


def create_expense_account_move(expense_sheet, move_date, project_code):
    print("expense_sheet", expense_sheet)
    amount_total = expense_sheet["total_amount"]
    amount_taxes = expense_sheet["taxed_amount"]
    amount_untaxed = expense_sheet["untaxed_amount"]

    MAIN_PROJECT_CHECKING_ACCOUNT_ID = odoo.accounts.get_account_id_from_code(
        odoo.accounts.get_odoo_account_code(odoo.accounts.get_main_project_code())
    )
    expense_sheet_name = expense_sheet["name"]
    print("project_code", project_code)
    project_account_id = odoo.accounts.get_account_id_from_code(
        odoo.accounts.get_odoo_account_code(project_code)
    )
    print("move_date", move_date)
    print("project_account_id", project_account_id)
    journal_id = int(settings.ODOO_JOURNAL_ID_FOR_INTERNAL_TRANSFERENCES)
    print("journal_id", journal_id)
    PROVIDERS_ACCOUNT_ID = odoo.accounts.get_account_id_from_code(
        settings.ODOO_ACCOUNT_EXPENSES_PAYMENT
    )
    print("MAIN_PROJECT_CHECKING_ACCOUNT_ID", MAIN_PROJECT_CHECKING_ACCOUNT_ID)
    print("PROVIDERS_ACCOUNT_ID", PROVIDERS_ACCOUNT_ID)
    line_ids = [
        (
            0,
            0,
            {
                "name": f"{expense_sheet_name}",
                "account_id": project_account_id,
                "credit": amount_untaxed,
            },
        )
    ]
    line_ids.append(
        (
            0,
            0,
            {
                "name": f"Impuestos {expense_sheet_name}",
                "account_id": MAIN_PROJECT_CHECKING_ACCOUNT_ID,
                "credit": amount_taxes,
            },
        )
    )

    line_ids.append(
        (
            0,
            0,
            {
                "name": f"Pago a proovedor: {expense_sheet_name}",
                "account_id": PROVIDERS_ACCOUNT_ID,
                "debit": amount_total,
            },
        )
    )

    move_data = {
        "journal_id": journal_id,  # Reemplaza con el ID real del diario
        "ref": expense_sheet["reference"],
        "date": move_date,
        "line_ids": line_ids,
    }
    print("move_data", move_data)

    account_move_id = odoo_server.Create("account.move", [move_data])
    if not account_move_id:
        print("Error creating account_move_id in Odoo Server")
        return None

    print("account_move_id", account_move_id)
    print("Movimiento creado")
    odoo_server.Method("account.move", "post", account_move_id)

    return account_move_id, None


def create_odoo_expense(data):
    odoo_expenses = []
    PROJECT_CODE = data["journal"]
    employee_id = data["odoo_employee_id"]
    expense_date = data["date"]
    journal_id_code_prefix = f"GASTOS {PROJECT_CODE}"
    journal_id = None
    journal_ids = odoo_server.SearchRead(
        "account.journal",
        [("name", "ilike", journal_id_code_prefix + "%")],
        fields=["id", "sequence_number_next"],
    )
    if not journal_ids:
        print(f"No journal_id found with code: {journal_id_code_prefix}")
        message = "ODOO_JOURNAL_NOT_FOUND_ERROR"
        print(message)
        return None, message
    else:
        journal = journal_ids[0]
        print("journal", journal)
        journal_id = journal["id"]
        sequence_number_next = journal["sequence_number_next"]
        print("sequence_number_next", sequence_number_next)
        print(f"Journal id for {PROJECT_CODE} found: {journal_id}")
        odoo_expenses = []
        for line in data["lines"]:
            product = odoo_server.SearchRead(
                "product.product",
                domain=[("default_code", "=", f"{PROJECT_CODE}-01")],
                order="id asc",
                fields=["id", "property_account_expense_id"],
            )
            account_id = (
                product[0]["property_account_expense_id"][0]
                if product[0]["property_account_expense_id"]
                else False
            )
            product_id = product[0]["id"]

            expense_data = {
                "reference": line["reference"],
                "name": line["description"],
                "product_id": product_id,  # ID of the product related to the expense
                "unit_amount": line["price_unit"],
                "quantity": line["quantity"],
                "date": line["date"],
                "account_id": account_id,  # ID of the expense account
                "employee_id": employee_id,  # ID of the employee
                "payment_mode": "own_account",
                "state": "draft",
            }

            # IDs of the taxes applied to the expense
            if "tax_id" in line and data["type_expense"] != "normal_expense":
                tax_ids = [int(line["tax_id"])]
                expense_data["tax_ids"] = [(6, 0, tax_ids)]
            else:
                expense_data["tax_ids"] = []

            # pprint(expense_data)

            try:
                odoo_expense = odoo_server.Create("hr.expense", [expense_data])
                print("Odoo_expense created", odoo_expense)
            except:
                message = "ODOO_CREATING_EXPENSE_ERROR"
                print(message)
                return None, message

            if odoo_expense:
                odoo_expense_id = odoo_expense[0]
                print("Odoo_expense created", odoo_expense_id)
                odoo_expenses.append(odoo_expense_id)
                if "uploaded_file" in line and line["uploaded_file"]:
                    add_odoo_atachment_to_resource(
                        line["uploaded_file"], odoo_expense_id
                    )

    # Creating Odoo expense.sheet
    expense_sheet_data = {
        "employee_id": employee_id,  # ID del empleado asociado
        "name": f"HG-{utils.CURRENT_YEAR}-{PROJECT_CODE}-{str(sequence_number_next).zfill(2)}",  # Nombre de la hoja de gastos, ej. #HG-2023-XXX-01
        "expense_line_ids": [
            (6, 0, odoo_expenses)
        ],  # Lista de IDs de objetos hr.expense
        "payment_mode": "own_account",
        "accounting_date": expense_date,
        "state": "approve",
        "journal_id": journal_id,
    }

    print("expense_sheet_data", expense_sheet_data)
    print("Creating hr.expense.sheet in Odoo")
    try:
        odoo_expense_sheet = odoo_server.Create(
            "hr.expense.sheet", [expense_sheet_data]
        )
        print("Odoo Id odoo_expense_sheet created", odoo_expense_sheet)
        return odoo_expense_sheet[0], False
    except:
        message = "ODOO_CREATING_EXPENSE_SHEET_ERROR"
        print(message)

    return None, message


def get_expense_sheet(expense_sheet_id):
    expense_sheet = odoo_server.Read("hr.expense.sheet", [expense_sheet_id])
    if expense_sheet:
        move_lines = []
        expense_sheet = expense_sheet[0]
        taxed_amount = 0
        for id in expense_sheet["expense_line_ids"]:
            move_line = odoo_server.Read(
                "hr.expense",
                id,
            )
            if move_line:
                move_line = move_line[0]
                taxed_amount += move_line["total_amount"] - move_line["untaxed_amount"]
                move_lines.append(move_line)

        total_amount = expense_sheet["total_amount"]
        taxed_amount = round(taxed_amount, 2)
        expense_sheet["taxed_amount"] = taxed_amount
        expense_sheet["untaxed_amount"] = total_amount - taxed_amount
        return expense_sheet, move_lines
    else:
        return None


def get_expenses_ids_allowed(user_id):
    employee_id = odoo.accounts.get_employee_id(user_id)
    own_expenses_ids = odoo_server.Search(
        "hr.expense.sheet", domain=[("employee_id", "=", employee_id)]
    )
    project_expenses_ids = []  # TODO
    return own_expenses_ids + project_expenses_ids


def get_expenses(user_id):
    items_result = []
    found = False
    fields_to_show = [
        "id",
        "name",
        "employee_id",
        "accounting_date",
        "state",
        "total_amount",
    ]
    user_record = odoo_server.SearchRead(
        "res.users", [("id", "=", user_id)], ["employee_id"]
    )
    if user_record:
        employee_id = user_record[0]["employee_id"]
        if employee_id:
            found = True
            employee_id = employee_id[0]  # get id
            items = odoo_server.SearchRead(
                "hr.expense.sheet",
                domain=[("employee_id", "=", employee_id)],
                order="id asc",
                fields=fields_to_show,
            )

    if not found:
        print(Fore.RED + f"Not Found employee_id for user_id : {user_id}")
        print(Style.RESET_ALL)
        return False

    if items:
        print(Fore.GREEN + f"Found user_id with : {user_id}")
        print(Style.RESET_ALL)

        for item in items:
            items_result.append(
                {
                    field: (
                        item[field]
                        if field in item and item[field] is not False
                        else ""
                    )
                    for field in fields_to_show
                }
            )

    return items_result


def get_expenses_from_journal(journal_id):
    items_result = []
    found = False
    fields_to_show = [
        "id",
        "name",
        "employee_id",
        "accounting_date",
        "state",
        "total_amount",
    ]
    journal_record = odoo_server.SearchRead(
        "account.journal", [("id", "=", journal_id)], []
    )
    if journal_record:
        found = True
        items = odoo_server.SearchRead(
            "hr.expense.sheet",
            domain=[("journal_id", "=", journal_id)],
            order="id asc",
            fields=fields_to_show,
        )
        print("items", items)

    if not found:
        print(Fore.RED + f"Not Found journal with ID: {journal_id}")
        print(Style.RESET_ALL)
        return False

    if items:
        print(Fore.GREEN + f"Found expenses for journal with ID: {journal_id}")
        print(Style.RESET_ALL)

        for item in items:
            items_result.append(
                {
                    field: (
                        item[field]
                        if field in item and item[field] is not False
                        else ""
                    )
                    for field in fields_to_show
                }
            )

    return items_result
