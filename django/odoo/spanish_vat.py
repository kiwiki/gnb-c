import re
from re import Match


def validar_vat_cif(vat_number):
    # Eliminar guiones y convertir a mayúsculas
    vat_number = vat_number.replace("-", "").replace(" ", "").upper()

    validacionNIF: bool = True
    esDNI: bool = False
    esNIE: bool = False
    esCIF: bool = False

    patronDNI: str = "^[0-9]{8}[A-Z]$"
    patronNIE: str = "^[X-Z][0-9]{7}[A-Z]$"
    patronCIF: str = "^[A-H|K-N|P-S|U-W][0-9]{8}$"

    tamano: int = len(vat_number)
    if tamano > 9:
        validacionNIF = False

    DNIValido: Match = re.match(patronDNI, vat_number)
    if DNIValido != None:
        esDNI = True

    NIEValido: Match = re.match(patronNIE, vat_number)
    if NIEValido != None:
        esNIE = True

    CIFValido: Match = re.match(patronCIF, vat_number)
    if CIFValido != None:
        esCIF = True

    if esDNI:
        digitosControl: str = "TRWAGMYFPDXBNJZSQVHLCKE"
        DNIInvalidos: list = ["00000000T", "00000001R", "99999999R"]
        for DNIInvalido in DNIInvalidos:
            if vat_number == DNIInvalido:
                validacionNIF = False

        numerosDNI: int = int(vat_number[0:8])
        ultimoDigito: str = vat_number[8]
        valorDigitoControl: int = numerosDNI % 23
        if ultimoDigito != digitosControl[valorDigitoControl]:
            validacionNIF = False

    if esNIE:
        digitosControl: str = "TRWAGMYFPDXBNJZSQVHLCKE"
        numerosNIE: int = int(vat_number[1:8])
        primerDigito: str = vat_number[0]
        ultimoDigito: str = vat_number[8]
        if primerDigito == "X":
            valorDigitoControl: int = numerosNIE % 23
            if ultimoDigito != digitosControl[valorDigitoControl]:
                validacionvat_number = False

        if primerDigito == "Y":
            valorDigitoControl: int = (10000000 + numerosNIE) % 23
            if ultimoDigito != digitosControl[valorDigitoControl]:
                validacionNIF = False

        if primerDigito == "Z":
            valorDigitoControl: int = (20000000 + numerosNIE) % 23
            if ultimoDigito != digitosControl[valorDigitoControl]:
                validacionNIF = False

    if esCIF:
        contador: int = 2
        sumaNumerosPares: int = 0
        sumaNumerosImpares: int = 0
        while contador <= 8:
            numeroImpar: int = int(vat_number[contador - 1])
            if contador != 8:
                sumaNumerosPares += int(vat_number[contador])
                dobleNumeroImpar: int = numeroImpar * 2
                if dobleNumeroImpar > 10:
                    numeroImpar = (dobleNumeroImpar % 10) + 1
                sumaNumerosImpares += numeroImpar
            else:
                dobleNumeroImpar: int = numeroImpar * 2
                if dobleNumeroImpar > 10:
                    numeroImpar = (dobleNumeroImpar % 10) + 1
                sumaNumerosImpares += numeroImpar
            contador += 2

        sumaTotal: int = sumaNumerosPares + sumaNumerosImpares
        digitoControl: int = 10 - (sumaTotal % 10)
        letraControl = chr(64 + digitoControl)
        primerDigito: str = vat_number[0]

        if (
            primerDigito == "A"
            or primerDigito == "B"
            or primerDigito == "E"
            or primerDigito == "H"
        ):
            ultimoDigito: int = int(vat_number[8])
            if ultimoDigito != digitoControl:
                validacionNIF = False
        if (
            primerDigito == "K"
            or primerDigito == "P"
            or primerDigito == "Q"
            or primerDigito == "S"
        ):
            ultimoDigito: str = vat_number[8]
            if ultimoDigito != letraControl:
                validacionNIF = False
    if esDNI == False and esNIE == False and esCIF == False:
        return False

    return vat_number
