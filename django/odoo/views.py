from colorama import Fore, Style, init
from pprint import pprint
import json
import base64
import traceback

from collections import defaultdict
import datetime

from django.http import (
    JsonResponse,
    HttpResponse,
    HttpResponseForbidden,
    HttpResponseNotFound,
)
from . import odoo_server
import odoo.utils
import odoo.accounts
import odoo.expenses
from odoo.models import OdooProfile

from gnb.models import Project, CashAccount

# from odoo.spanish_vat import validar_vat_cif
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import View
from django.contrib.auth.models import User
from django.conf import settings


init()


def get_tax_id_number(request, tax_id_number):
    print(Fore.YELLOW + f"Searching tax_id number {tax_id_number} in Odoo")
    print(Style.RESET_ALL)
    # tax_id_number = validar_vat_cif(tax_id_number)
    if not tax_id_number:
        partner_json = {
            "error": "NOT_VALID_VAT_ID",
        }
    else:
        partner_ids = odoo_server.Search(
            "res.partner", [("vat", "ilike", tax_id_number)]
        )
        if len(partner_ids) > 0:
            print(Fore.GREEN + f"Found client with : {tax_id_number}")
            print(Style.RESET_ALL)
            fields_to_show = [
                "id",
                "name",
                "email",
                "street",
                "street2",
                "city",
                "state_id",
                "vat",
                "zip",
                "phone",
                "country_id",
            ]
            partners = odoo_server.Read(
                "res.partner", partner_ids, fields=fields_to_show
            )
            partner = partners[0]
            print(partner)
            partner_json = {
                field: (
                    partner[field]
                    if field in partner and partner[field] is not False
                    else ""
                )
                for field in fields_to_show
            }
        else:
            print(Fore.RED + f"Not Found client with : {tax_id_number}")
            print(Style.RESET_ALL)
            partner_json = {
                "id": None,
            }

    print(Style.RESET_ALL)
    return JsonResponse(partner_json)


@login_required
def delete_account_move(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    else:  # an admin is logging as that user
        # Obteniendo el ID del asiento contable que deseas eliminar a partir del nombre
        # ('state', '=', 'posted')
        move_name = request.GET.get("move_name")
        if not move_name:
            return HttpResponseNotFound(
                "Añade el parámetro ?move_name=XXX/XXX/XXX para borrar ese account move"
            )
        else:
            account_moves = odoo_server.Search(
                "account.move", domain=[("name", "=", move_name)], order="id asc"
            )
            if not account_moves:
                return HttpResponseNotFound(
                    f"No se ha encontrado el asiento contable <b>{move_name}</b>"
                )
            else:
                account_move_id = account_moves[0]  # Reemplaza con el ID correcto
                # Utiliza el método unlink para eliminar el registro del modelo account.move
                print("Account move Id found:", account_move_id)
                response = odoo_server.Write(
                    "account.move", [account_move_id], {"state": "cancel"}
                )  # Pasa a pagado
                print(f"Response cancel: {response}")
                response = odoo_server.Unlink("account.move", [account_move_id])
                print(f"Response unlink: {response}")
                if response:
                    return HttpResponse(
                        f"Éxito eliminando el asiento contable <b>{move_name}</b>.<br><img src='https://media.tenor.com/CnP64S7lszwAAAAi/meme-cat-cat-meme.gif'/> "
                    )
                else:
                    return HttpResponse(
                        f"ERROR eliminando el asiento contable <b>{move_name}</b>.<br>",
                        status=500,
                    )


@login_required
def validate_invoice(request, invoice_id):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    else:
        odoo_server.Method("account.move", "post", invoice_id)
        invoice_details = odoo_server.Read(
            "account.move", [invoice_id], ["name", "invoice_user_id"]
        )
        if invoice_details:
            details = {
                "invoice_id": invoice_id,
                "invoice_name": invoice_details[0]["name"],
            }
            invoice_user_id = invoice_details[0]["invoice_user_id"][0]
            user = User.objects.filter(
                odooprofile__odoo_user_id=invoice_user_id
            ).first()
            print("user", user)
            if user:
                details["email_to"] = user.email
                odoo.utils.send_email_invoice_validated(details)
            OdooProfile.objects.filter(odoo_user_id=invoice_user_id)
            return HttpResponse(
                "Factura validada y notificaciones enviadas. <br><img src='https://media.tenor.com/CnP64S7lszwAAAAi/meme-cat-cat-meme.gif'/>",
                status=200,
            )


@login_required
def validate_expense_sheet(request, expense_sheet_id):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    else:  # an admin is logging as that user
        odoo_server.Method(
            "hr.expense.sheet", "action_sheet_move_create", expense_sheet_id
        )  # Publicar asiento contable

        record_details = odoo_server.Read(
            "hr.expense.sheet", [expense_sheet_id], ["expense_line_ids", "name"]
        )
        record_details = record_details[0]
        print("record_details", record_details)
        line_ids = odoo_server.SearchRead(
            "hr.expense",
            domain=[("id", "in", record_details["expense_line_ids"])],
            fields=["unit_amount", "quantity"],
        )
        total_amount_before_tax = 0
        for line in line_ids:
            total_amount_before_tax += line["unit_amount"] * line["quantity"]

        hs_name = record_details["name"]
        project_code = hs_name.split("-")[2]  # code is G.XXX, we want XXX
        journals = odoo_server.SearchRead(
            "account.journal",
            [("code", "like", project_code), ("type", "in", ["bank"])],
            fields=["id", "name"],
        )
        if journals:
            _ = journals[0]["id"]
        else:
            return HttpResponse(
                f"ERROR encontrando el journal id de tipo banco del proyecto {project_code}"
            )

        print("journal", journals)
        project_data = (
            Project.objects.filter(code=project_code).values("account_checking").first()
        )

        if not project_data:
            return HttpResponse(
                "ERROR encontrando el proyecto con el journal id de la hoja de gastos",
                status=400,
            )
        else:
            object_id = True
            message = False
            if object_id:
                _ = odoo_server.Write(
                    "hr.expense.sheet", [expense_sheet_id], {"state": "done"}
                )  # Pasa a pagado
                item_details = odoo_server.Read(
                    "hr.expense.sheet", [expense_sheet_id], ["name", "employee_id"]
                )
                if item_details:
                    details = {
                        "expense_sheet_id": expense_sheet_id,
                        "expense_sheet_name": item_details[0]["name"],
                    }
                    employee_id = item_details[0]["employee_id"][0]
                    odoo_user_id = odoo.accounts.get_odoo_user_from_employee(
                        employee_id
                    )
                    if odoo_user_id is None:
                        return HttpResponseNotFound(
                            "Employee id or Odoo User id associated found."
                        )
                    else:
                        user = User.objects.filter(
                            odooprofile__odoo_user_id=odoo_user_id
                        ).first()
                        if user:
                            details["email_to"] = user.email
                            odoo.utils.send_email_expense_sheet_validated(details)

                return HttpResponse(
                    "Éxito cambiando la hoja de gastos a <b>Done</b>.<br><img src='https://media.tenor.com/CnP64S7lszwAAAAi/meme-cat-cat-meme.gif'/> ",
                    status=200,
                )
            else:
                return HttpResponse(
                    "ERROR creando el asiento contable al validar la hoja de gastos.<br>"
                    + message,
                    status=500,
                )

        return HttpResponse(
            "ERROR cambiando la hoja de gastos a <b>Done</b>. la transferencia.",
            status=400,
        )


@method_decorator(
    csrf_exempt, name="dispatch"
)  # Use this decorator if you want to disable CSRF protection (for testing purposes)
class InvoiceView(View):
    def post(self, request):
        try:
            data = json.loads(
                request.body.decode("utf-8")
            )  # Parse the JSON data from the request
        except json.JSONDecodeError as e:
            return JsonResponse({"error": "Invalid JSON data"}, status=400)

        user = request.user
        user_id = data.get("user_id", None)
        if not user_id:
            # If empty, get the user logged in's own invoices
            user_id = user.id
        else:  # an admin is logging as that user
            user_id = int(user_id)
            if user.id != user_id and not request.user.is_superuser:
                # if non admin user chooses a different user id in the params
                return HttpResponseForbidden("Access denied. Event reported to admins.")
            else:
                user = User.objects.get(id=user_id)

        odoo_profile = user.odooprofile
        odoo_user_id = odoo_profile.odoo_user_id

        cleaned_data = clean_odoo_form_data(request, data)
        cleaned_data["odoo_user_id"] = odoo_user_id
        client_vat = data["taxIdInput"]
        client_name = data["client_name"]
        # Return a JSON response if needed
        invoice_id, message = create_invoice(cleaned_data)
        if invoice_id:
            details = {
                "username": request.user.username,
                "invoice_id": invoice_id,
                "client_name": client_name,
                "client_vat": client_vat,
            }
            try:
                odoo.utils.send_email_new_invoice_created(details)
            except:
                return JsonResponse(
                    {"error": "Factura creada pero debes comunicar a l@s admins."},
                    status=200,
                )
        return JsonResponse({"invoice_id": invoice_id, "message": message})


def clean_odoo_form_data(request, data):
    rows_dict = defaultdict(dict)
    rows = []
    new_data = {}
    for key, value in data.items():
        if "-" not in key:
            new_data[key] = value
        else:
            n = key.split("-")[1]
            if key.startswith("quantity"):
                rows_dict[n]["quantity"] = value
            elif key.startswith("price"):
                rows_dict[n]["price_unit"] = value
            elif key.startswith("taxes"):
                rows_dict[n]["tax_id"] = value
            elif key.startswith("description"):
                rows_dict[n]["description"] = value
            elif key.startswith("product"):
                rows_dict[n]["product_id"] = value
            elif key.startswith("date_line"):
                rows_dict[n]["date"] = value
            elif key.startswith("reference"):
                rows_dict[n]["reference"] = value

    if request.FILES:
        for key, uploaded_file in request.FILES.items():
            if "-" in key:
                n = key.split("-")[1]
                rows_dict[n]["uploaded_file"] = uploaded_file

    sorted_keys = sorted(rows_dict.keys())
    rows = [rows_dict[key] for key in sorted_keys]

    new_data["lines"] = rows
    if not "date" in data:
        new_data["date"] = datetime.datetime.now().date().strftime("%Y-%m-%d")

    return new_data


def create_invoice(data):

    # tax_0percent = 34 #Impuesto 0% Exento.

    id_tax_number = data["taxIdInput"]
    journal_name = data["journal"]
    # TODO add the type of account.journal to be sale
    journal_ids = odoo_server.Search("account.journal", [("code", "=", journal_name)])
    if not journal_ids:
        message = "ODOO_NOT_JOURNAL_ID_FOUND_ERROR"
        return None, message
    else:
        journal_id = journal_ids[0]

    clients = odoo_server.Search("res.partner", [("vat", "=", id_tax_number)])
    if len(clients):
        client_id = clients[0]
    else:
        message = f"Not Found Odoo partner vat : {id_tax_number}. Trying to create."
        # id_tax_number = validar_vat_cif(id_tax_number)
        if not id_tax_number:
            message = "ODOO_INCORRECT_VAT_ID_ERROR"
            return None, message
        else:
            # Create the Partner in Odoo
            new_partner = {
                "vat": id_tax_number,
                "name": data["client_name"],
                "street": data["client_street"],
                "city": data["client_city"],
                "zip": data["client_zip"],
            }
            client_id = odoo_server.Create("res.partner", new_partner)
            if not client_id:
                message = "ODOO_CREATING_CLIENT_ERROR"
                return None, message
            else:
                new_partner["client_id"] = client_id
                odoo.utils.send_email_new_client_created(new_partner)

    invoice_line_ids = []
    for line in data["lines"]:
        product_id = line[
            "product_id"
        ]  # Assuming there is only one product with the specified code
        product = odoo_server.SearchRead(
            "product.product",
            domain=[("id", "ilike", product_id)],
            order="id asc",
            fields=["property_account_income_id"],
        )
        account_id = (
            product[0]["property_account_income_id"][0]
            if product[0]["property_account_income_id"]
            else False
        )
        if not account_id:
            message = "ODOO_ACCOUNT_ID_NOT_FOUND_FOR_PRODUCT"
            return None, message

        tax_ids = [(6, 0, [int(line["tax_id"])])]
        invoice_line_id = (
            0,
            0,
            {
                "name": line["description"],  # Description of the line
                "account_id": account_id,  # ID of the account for the line
                "product_id": line["product_id"],
                "quantity": line["quantity"],
                "tax_ids": tax_ids,
                "price_unit": line["price_unit"],
            },
        )
        invoice_line_ids.append(invoice_line_id)

    if "note" in data and data["note"]:
        invoice_line_id = (
            0,
            0,
            {
                "display_type": "line_note",
                "name": data["note"],
                "product_id": None,
                "account_id": None,
                "tax_ids": None,
                "quantity": 1,
                "price_unit": 0,
            },
        )
        invoice_line_ids.append(invoice_line_id)

    # Creating invoice
    invoice_date_str = data["date"]

    # La variable terminos de pago está mal... corregir
    # payment_term_id = int(data['terminos-pago'])
    move_vals = {
        "invoice_user_id": int(data["odoo_user_id"]),
        "type": "out_invoice",
        "partner_id": client_id,
        "invoice_date": invoice_date_str,  # Date of the move
        "journal_id": journal_id,  # ID of the journal to which the move belongs
        "invoice_line_ids": invoice_line_ids,
        "ref": data["referencia"],
        "state": "draft",
        "invoice_payment_term_id": [(6, 0, 1)],
    }
    message = "ODOO_CREATING_INVOICE_ERROR"
    invoice_id = odoo_server.Create("account.move", move_vals)
    if invoice_id:
        message = False

    return invoice_id, message


def add_odoo_atachment_to_resource(uploaded_file, resource_id):
    # Use the Odoo API to create an attachment
    binary_data = base64.b64encode(uploaded_file.read())
    # Convert binary data to ASCII string using 'latin1' encoding
    utf_data = binary_data.decode("utf-8")
    attachment_data = {
        "name": uploaded_file.name,  # Use the uploaded file's name
        "res_name": "Expense Description",  # Set a name for the related record (expense description)
        "res_model": "hr.expense",  # Set the model to which the attachment is related
        "res_id": resource_id,  # Set the ID of the related record
        "type": "binary",
        "datas": utf_data,  # Read the file data
    }

    # Create the attachment in Odoo
    attachments = odoo_server.Create("ir.attachment", [attachment_data])
    print("attachments created:", attachments)


# Use this decorator if you want to disable CSRF protection (for testing purposes)
@method_decorator(csrf_exempt, name="dispatch")
class ExpenseView(View):
    def post(self, request):
        try:
            data = request.POST  # Parse the JSON data from the request
        except:
            return JsonResponse({"error": "Error reading post data"}, status=400)

        user_id = data.get("user_id", None)
        user = request.user
        if not user_id:
            # If empty, get the user logged in's own invoices
            user_id = user.id
        else:  # an admin is logging as that user
            user_id = int(user_id)
            if user.id != user_id and not request.user.is_superuser:
                # if non admin user chooses a different user id in the params
                return HttpResponseForbidden("Access denied. Event reported to admins.")
            else:
                user = User.objects.get(id=user_id)

        odoo_profile = user.odooprofile
        odoo_employee_id = odoo_profile.get_employee_id()
        iban_account = data.get("iban_account", "")
        iban_receiver = data.get("iban_receiver", "")
        iban_concept = data.get("iban_concept", "")

        cleaned_data = clean_odoo_form_data(request, data)
        cleaned_data["odoo_employee_id"] = odoo_employee_id
        # Return a JSON response if needed
        hr_expense_sheet_id, message = odoo.expenses.create_odoo_expense(cleaned_data)
        if hr_expense_sheet_id:
            expense_details, _ = odoo.expenses.get_expense_sheet(hr_expense_sheet_id)
            details = {
                "username": request.user.username,
                "onbehalf_username": user.username,
                "expense_details": expense_details,
                "hr_expense_sheet_id": hr_expense_sheet_id,
                "iban_account": iban_account,
                "iban_receiver": iban_receiver,
                "iban_concept": iban_concept,
            }

            odoo.utils.send_email_new_expense_created(details)

            if cleaned_data["type_expense"] != "normal_expense" and iban_account:
                project_code = cleaned_data["journal"]
                expense_details["reference"] = cleaned_data["lines"][0]["description"]
                object_id, message = odoo.expenses.create_expense_account_move(
                    expense_details, cleaned_data["date"], project_code
                )

                if object_id:
                    details = {
                        "username": request.user.username,
                        "onbehalf_username": user.username,
                        "object_id": object_id[0],
                        "from_account": project_code,
                        "to_account": "Proveedores",
                        "amount": expense_details["untaxed_amount"],
                        "reference": expense_details["name"],
                        "expense_details": expense_details,
                    }

                    odoo.utils.send_email_new_account_move(details)
        # Total va a debito va cuenta a proveedores
        # el subtotal sin impuestos va a la cuenta del proyecto
        # los impuestos a la cuenta general

        return JsonResponse({"hr_expense_id": hr_expense_sheet_id, "message": message})


@method_decorator(login_required, name="dispatch")
@method_decorator(csrf_exempt, name="dispatch")
class CashAccountMoveView(View):
    def post(self, request):
        data = request.POST  # Parse the JSON data from the request

        cash_account_id = data.get("cash_account_id", None)
        if not cash_account_id:
            return JsonResponse(
                {"error": "CashAccount non existent."},
                status=400,
            )
        user = request.user
        cash_account = CashAccount.objects.get(id=cash_account_id)
        is_admin = user in cash_account.user_admins.all()

        if not is_admin and not request.user.is_superuser:
            # if non admin user chooses a different user id in the params
            return JsonResponse(
                {"error": "Access denied, reported to admins."},
                status=403,
            )

        tx_amount = data.get("tx_amount", None)
        tx_direction = data.get("tx_direction", None)  # can be in our out
        tx_reference = data.get("tx_reference", None)
        if tx_direction == "in":
            account_debit = cash_account.account_number
            account_credit = settings.ODOO_ACCOUNT_CLIENTS
        else:
            account_debit = settings.ODOO_ACCOUNT_EXPENSES_PAYMENT
            account_credit = cash_account.account_number

        tx_amount = float(tx_amount)
        if (
            None in [tx_amount, tx_reference, account_debit, account_credit]
            or tx_amount <= 0
        ):
            return JsonResponse(
                {"error": "Error with transaccion parameters."}, status=400
            )

        object_id, message = odoo.accounts.create_bank_account_move(
            account_debit, account_credit, tx_amount, tx_reference
        )

        if object_id:
            details = {
                "username": request.user.username,
                "object_id": object_id[0],
                "cash_account": cash_account,
                "amount": tx_amount,
                "project": cash_account.project,
                "reference": tx_reference,
                "tx_direction": tx_direction,
            }

            odoo.utils.send_email_new_cash_account_move(details)

        return JsonResponse({"object_id": object_id, "message": message})


@method_decorator(login_required, name="dispatch")
@method_decorator(csrf_exempt, name="dispatch")
class BankAccountMoveView(View):
    def post(self, request):
        try:
            data = request.POST  # Parse the JSON data from the request
        except Exception as e:
            return JsonResponse(
                {"object_id": None, "error": "Error reading post data"}, status=400
            )

        print("POST data", data)
        account_credit = None
        account_debit = None
        user_id = data.get("user_id", None)
        user = request.user
        if not user_id:
            # If empty, get the user logged in's own invoices
            user_id = user.id
        else:  # an admin is logging as that user
            user_id = int(user_id)
            if user.id != user_id and not request.user.is_superuser:
                # if non admin user chooses a different user id in the params
                return HttpResponseForbidden("Access denied. Event reported to admins.")
            else:
                user = User.objects.get(id=user_id)

        odoo_profile = user.odooprofile

        from_account = data.get("from_account", None)
        if from_account:
            if from_account == "personal_account":
                from_account = user.email
                account_credit = odoo_profile.account_checking
            else:
                account_credit = odoo.accounts.get_odoo_account_code(from_account)

        to_account_internal = data.get("to_account_internal", None)
        if to_account_internal:
            to_account = to_account_internal
            if to_account_internal == "personal_account":
                to_account = user.email
                account_debit = odoo_profile.account_checking
            else:
                account_debit = odoo.accounts.get_odoo_account_code(to_account_internal)

        avoid_notify = (
            True
            if "avoid_notify" in request.POST
            and request.POST.get("avoid_notify") == "on"
            else False
        )
        to_account_external = data.get("to_account_external", None)
        if to_account_external:
            account_debit = settings.ODOO_ACCOUNT_EXPENSES_PAYMENT
            to_account = to_account_external
            avoid_notify = True  # if external transfer, we don't have anybody to notify

        amount = data.get("transfer_amount", None)
        amount = float(amount)
        reference = data.get("transfer_ref", None)
        if None in [amount, reference, account_debit, account_credit] or amount <= 0:
            return JsonResponse(
                {"error": "Incorrect transaction parameters. Reported to admins."},
                status=400,
            )
        object_id, message = odoo.accounts.create_bank_account_move(
            account_debit, account_credit, amount, reference
        )

        if object_id:
            details = {
                "username": request.user.username,
                "onbehalf_username": user.username,
                "object_id": object_id[0],
                "from_account": from_account,
                "to_account": to_account,
                "amount": amount,
                "reference": reference,
            }

            odoo.utils.send_email_new_account_move(details)
            if not avoid_notify:
                odoo.utils.send_email_new_account_move_to_recipients(details)

        return JsonResponse({"object_id": object_id[0], "message": message})
