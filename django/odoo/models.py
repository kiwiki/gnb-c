import json

from django.db import models
from django.contrib.auth.models import User
import odoo.accounts
from django.utils.translation import gettext_lazy as _


class OdooProfile(models.Model):
    user = models.OneToOneField(
        User,
        primary_key=True,
        on_delete=models.CASCADE,
    )
    membership_number = models.CharField(
        "Número de soci@", max_length=100, null=True, blank=True
    )
    odoo_user_id = models.IntegerField(unique=True)
    data_json = models.TextField(blank=True)

    account_checking = models.IntegerField(
        _("Cuenta corriente"), unique=True, null=True, blank=True
    )
    account_cash = models.IntegerField(
        _("Cuenta efectivo"), unique=True, null=True, blank=True
    )
    account_voluntary_contribution = models.IntegerField(
        _("Cuenta contribución voluntaria"), unique=True, null=True, blank=True
    )
    account_social_capital = models.IntegerField(
        _("Cuenta capital social"), unique=True, null=True, blank=True
    )
    account_loans = models.IntegerField(
        _("Cuenta préstamos"), unique=True, null=True, blank=True
    )
    reimbursable_social_capital = models.FloatField(
        blank=True, default=48, null=True, verbose_name="Capital Social Rembolsable"
    )

    def set_data_json(self, list_projects):
        self.projects_json = json.dumps(list_projects)

    def get_data_json(self):
        return json.loads(self.projects_json)

    def get_employee_id(self):
        return odoo.accounts.get_employee_id(self.odoo_user_id)

    def get_partner_id(self):
        return odoo.accounts.get_partner_id(self.odoo_user_id)
