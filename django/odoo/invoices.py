from colorama import Fore, Back, Style, init
from pprint import pprint
from . import odoo_server
import odoo.accounts

init()


def register_payment(invoice_name, from_journal, payment_date):
    print("payment_date", payment_date)
    invoices_id = odoo_server.SearchRead(
        "account.move", [("name", "like", invoice_name)], order="id desc", fields=["id"]
    )
    if invoices_id:
        invoice_id = invoices_id[0]["id"]
        print("Registering payment for invoice:", invoice_id)
        payment_method_id = 1  # manual method
        data = [
            {
                "journal_id": from_journal,
                "payment_date": payment_date,
                "payment_method_id": payment_method_id,
                "invoice_ids": [(4, invoice_id)],
            }
        ]
        payment_register_id = odoo_server.Create("account.payment.register", data)
        print("payment_register_id", payment_register_id)
        result = odoo_server.Method(
            "account.payment.register", "create_payments", payment_register_id
        )
        if result:
            result = result["res_id"]
        return result
    else:
        return False


def get_project_from_invoice_name(invoice_name):
    parts = invoice_name.split("/")
    if parts:
        project_code = parts[1]
        return project_code


def get_invoice_by_name(invoice_name):
    invoices = odoo_server.SearchRead(
        "account.move", domain=[("name", "=", invoice_name)], order="id asc"
    )
    if invoices:
        return invoices[0]
    else:
        return None


def get_invoice(invoice_id):
    fields = [
        "name",
        "quantity",
        "price_unit",
        "tax_ids",
        "display_type",
        "price_subtotal",
        "sequence",
    ]
    print("SearchRead account.move id=", invoice_id)
    invoice = odoo_server.Read("account.move", invoice_id)

    if invoice and len(invoice) > 0:
        invoice = invoice[0]
        move_lines = []
        for id in invoice["invoice_line_ids"]:
            move_line = odoo_server.Read("account.move.line", id, fields)
            if move_line:
                move_lines.append(move_line[0])
        move_lines_sorted = sorted(move_lines, key=lambda x: x.get("sequence", 0))
        return invoice, move_lines_sorted
    else:
        return None, None


def get_partner(partner_id):
    client = odoo_server.Read("res.partner", [partner_id])
    if client:
        return client[0]
    else:
        return None


def get_invoices_ids_allowed(user_id):
    own_invoices_ids = odoo_server.Search(
        "account.move", domain=[("invoice_user_id", "=", user_id)]
    )
    project_invoices_ids = []  # TODO
    return own_invoices_ids + project_invoices_ids


def get_invoices_by_project(project_code):
    fields_to_show = [
        "id",
        "name",
        "partner_id",
        "invoice_user_id",
        "amount_total_signed",
        "amount_residual_signed",
        "state",
        "invoice_date",
        "invoice_payment_state",
    ]
    invoices = odoo_server.SearchRead(
        "account.move",
        domain=[("name", "like", f"/{project_code}/")],
        order="id asc",
        fields=fields_to_show,
    )

    if invoices and len(invoices) > 0:
        return invoices
    else:
        print(Fore.RED + f"Not Found invoices for project : {project_code}")
        print(Style.RESET_ALL)
        return []


def get_invoices_by_user(invoice_user_id):
    invoices_result = []
    fields_to_show = [
        "id",
        "name",
        "partner_id",
        "invoice_user_id",
        "amount_total_signed",
        "amount_residual_signed",
        "state",
        "invoice_date",
        "journal_id",
    ]
    invoices = odoo_server.SearchRead(
        "account.move",
        domain=[("invoice_user_id", "=", invoice_user_id)],
        order="id asc",
        fields=fields_to_show,
    )

    if invoices and len(invoices) > 0:
        print(Fore.GREEN + f"Found invoice_user_id with : {invoice_user_id}")
        print(Style.RESET_ALL)

        for invoice in invoices:
            journal_id = invoice["journal_id"][0]
            project_code = odoo.accounts.get_code_from_journal_id(journal_id)
            item = {
                field: (
                    invoice[field]
                    if field in invoice and invoice[field] is not False
                    else ""
                )
                for field in fields_to_show
            }
            item["project_code"] = project_code
            invoices_result.append(item)
    else:
        print(Fore.RED + f"Not Found invoice_user_id with : {invoice_user_id}")
        print(Style.RESET_ALL)

    print(invoices_result)
    return invoices_result


def get_all_unpaid_invoices():
    invoices_result = []
    fields_to_show = ["id", "name", "amount_residual_signed"]
    invoices = odoo_server.SearchRead(
        "account.move",
        domain=[
            ("state", "=", "posted"),
            ("type", "=", "out_invoice"),
            ("invoice_payment_state", "!=", "paid"),
        ],
        order="id asc",
        fields=fields_to_show,
    )
    if not invoices:
        invoices = []
    for invoice in invoices:
        invoice_data = {
            field: (
                invoice[field]
                if field in invoice and invoice[field] is not False
                else ""
            )
            for field in fields_to_show
        }
        invoice_data["project"] = invoice["name"].split("/")[1]
        invoices_result.append(invoice_data)

    return invoices_result


def get_products(projects):
    products = []
    pprint(projects)
    fields_to_show = [
        "default_code",
        "id",
        "name",
        "property_account_income_id",
        "taxes_id",
    ]
    for PROJECT_CODE in projects:
        product_code_prefix = f"{PROJECT_CODE[0:3]}"  # Replace with the actual product code or any other criteria
        products_odoo = odoo_server.SearchRead(
            "product.product",
            domain=[("default_code", "ilike", product_code_prefix + "-01")],
            order="id asc",
            fields=fields_to_show,
        )

        if products_odoo and len(products_odoo) > 0:
            for p in products_odoo:
                products.append(p)
        else:
            print(Fore.RED + f"Not Found products with : {PROJECT_CODE}")
            print(Style.RESET_ALL)

    pprint(products)
    return products


def get_next_invoice_number(project_code, year):
    invoices = odoo_server.Search(
        "account.move", [("name", "like", f"{year}/{project_code}/")], order="id desc"
    )
    max_invoice_number = 0
    if invoices:
        objects = odoo_server.Read(
            "account.move", invoices, fields=["name", "amount_untaxed", "amount_total"]
        )

        for invoice in objects:
            # ref_id is YEAR/CODE/NUMBER
            ref_id = invoice["name"]
            try:
                invoice_number = int(ref_id.split("/")[2])
                # print(f"{ref_id} Neto: {invoice['amount_untaxed']}€, Total: {invoice['amount_total']}€")
                if max_invoice_number < invoice_number:
                    max_invoice_number = invoice_number
            except:
                raise ValueError(
                    f"Invalid string format {ref_id}: Expected 'YEAR/CODE/NUMBER'"
                )

    formatted_max_invoice_number = str(max_invoice_number + 1).zfill(3)
    print(f"max_invoice_number is {max_invoice_number}")
    return f"{year}/{project_code}/{formatted_max_invoice_number}"
