from colorama import Fore, Back, Style, init
from pprint import pprint

from django.contrib.auth.models import User
from . import odoo_server
from gnb import utils

from collections import defaultdict
from odoo.models import OdooProfile

from django.conf import settings
from django.core.cache import cache

init()


def get_main_project_code():
    from gnb.models import Project

    main_project = Project.objects.filter(is_main_project=True).first()
    if main_project:
        return main_project.code
    return None


def get_total_balances(account_code, account_type="normal"):
    total_balance = None
    if account_code is not None:
        account_id = get_account_id_from_code(account_code)
        print(f"get_total_balances_{account_id}_{account_type}")

        count_movelines = odoo_server.SearchCount(
            "account.move.line",
            domain=[
                ("account_id", "=", account_id),
                ("parent_state", "!=", "cancel"),
                ("parent_state", "=", "posted"),
            ],
        )
        if not count_movelines:
            total_balance = 0
        else:
            total_balance = cache.get(
                f"get_total_balances_{account_id}_{count_movelines}"
            )

            if total_balance is not None:
                print(
                    f"Found cached get_total_balances_{account_id}_{count_movelines} = {total_balance}"
                )
            else:
                movelines = odoo_server.SearchRead(
                    "account.move.line",
                    domain=[
                        ("account_id", "=", account_id),
                        ("parent_state", "!=", "cancel"),
                        ("parent_state", "=", "posted"),
                    ],
                    order="id asc",
                    fields=["debit", "credit"],
                )
                total_balance = 0
                for moveline in movelines:
                    if account_type == "normal":
                        total_balance += moveline["credit"] - moveline["debit"]
                    elif account_type == "bank":
                        total_balance += moveline["debit"] - moveline["credit"]

                if settings.CACHE_TIME:
                    print(
                        f"Setting cached get_total_balances_{account_id}_{count_movelines} =  {total_balance}"
                    )
                    cache.set(
                        f"get_total_balances_{account_id}_{account_type}",
                        total_balance,
                        settings.CACHE_TIME,
                    )
                    cache.set(
                        f"get_total_balances_{account_id}_{count_movelines}",
                        total_balance,
                        settings.CACHE_TIME,
                    )
    return total_balance


def get_journal_name(journal_code):
    journals = odoo_server.SearchRead(
        "account.journal", [("code", "=", journal_code)], fields=["name"]
    )
    if not journals:
        print("Journal no encontrado")
        return None

    journal_name = journals[0]["name"]
    return journal_name


def get_made_donations(project_code, project_txs):
    total_donated = 0
    main_project_code = get_main_project_code()
    if main_project_code:
        for tx in project_txs:
            if (
                "receiver" in tx
                and tx["receiver"] == main_project_code
                and "sender" in tx
                and tx["sender"] == project_code
            ):
                total_donated += tx["amount_total"]

    return total_donated


def get_project_balances(invoices_journal_code, expenses_journal_code):
    total_unpaid_expenses = 0
    total_invoices_untaxed = 0
    journal_name = get_journal_name(invoices_journal_code)
    if journal_name:
        invoices = odoo_server.SearchRead(
            "account.move",
            [
                ("journal_id", "like", journal_name),
                ("state", "=", "posted"),
                ("state", "!=", "cancel"),
                ("invoice_payment_state", "=", "not_paid"),
            ],
            order="id desc",
            fields=["invoice_payment_state", "name", "amount_untaxed", "amount_total"],
        )
        total_invoices = 0
        for invoice in invoices:
            total_invoices_untaxed += invoice["amount_untaxed"]
            total_invoices += invoice["amount_total"]

        # print('total_untaxed', total_invoices_untaxed)

    journal_name = get_journal_name(expenses_journal_code)
    if journal_name:
        invoices = odoo_server.SearchRead(
            "hr.expense.sheet",
            [("journal_id", "like", journal_name), ("state", "!=", "done")],
            order="id desc",
            fields=["state", "name", "total_amount"],
        )

        for invoice in invoices:
            total_unpaid_expenses += invoice["total_amount"]

        # print('total', total_unpaid_expenses)

    return total_invoices_untaxed, total_unpaid_expenses


def get_account_id_from_journal(journal_id):
    account_item = odoo_server.SearchRead(
        "account.journal",
        domain=[("id", "=", journal_id)],
        fields=["default_debit_account_id"],
        order="id asc",
        limit=1,
    )
    if account_item:
        account_id = account_item[0]["default_debit_account_id"][0]
        return account_id
    else:
        return None


def get_code_from_journal_id(journal_id):
    journal_code = cache.get(f"get_code_from_journal_id_{journal_id}")
    if journal_code is None:
        journal_item = odoo_server.SearchRead(
            "account.journal",
            domain=[("id", "=", journal_id)],
            fields=["code"],
            order="id asc",
            limit=1,
        )
        if journal_item:
            journal_code = journal_item[0]["code"]
            if settings.CACHE_TIME:
                print(f"Setting cached get_code_from_journal_id_{journal_id}")
                cache.set(
                    f"get_code_from_journal_id_{journal_id}",
                    journal_code,
                    settings.CACHE_TIME,  # Longer cache
                )
            else:
                print(f"Found cached get_account_id_from_code_{journal_id}")
        else:
            return None
    return journal_code


def get_journal_id_from_code(journal_code):
    journal_item = odoo_server.SearchRead(
        "account.journal",
        domain=[("code", "=", journal_code)],
        fields=["id"],
        order="id asc",
        limit=1,
    )
    if journal_item:
        journal_id = journal_item[0]["id"]
        return journal_id
    else:
        return None


def get_code_from_account_id(account_id):
    account_item = odoo_server.SearchRead(
        "account.account",
        domain=[("id", "=", account_id)],
        fields=["code"],
        order="id asc",
        limit=1,
    )
    if account_item:
        account_code = account_item[0]["code"]
        return account_code
    else:
        return None


def get_account_id_from_code(account_code):
    account_id = cache.get(f"get_account_id_from_code_{account_code}")
    if account_id is None:
        account_id = odoo_server.Search(
            "account.account", [("code", "=", str(account_code))], limit=1
        )
        account_id = account_id[0] if account_id else False

        if settings.CACHE_TIME:
            print(
                f"Setting cached get_account_id_from_code_{account_code} = {account_id}"
            )
            cache.set(
                f"get_account_id_from_code_{account_code}",
                account_id,
                settings.CACHE_TIME * 30,  # Longer cache
            )
    else:
        print(f"Found cached get_account_id_from_code_{account_code} = {account_id}")

    return account_id


def get_transactions(account_code):
    tx_result = []
    account_code = str(account_code)
    account_id = get_account_id_from_code(account_code)
    count_account_move_lines = odoo_server.SearchCount(
        "account.move.line", domain=[("account_id", "=", account_id)]
    )
    if not count_account_move_lines:
        print(f"Not transactions found for account_code = {account_code}")
    else:
        tx_result = cache.get(
            f"get_transactions_{account_code}_{count_account_move_lines}"
        )
        if tx_result is not None:
            print(
                f"Found cache from get_transactions_{account_code}_{count_account_move_lines}"
            )
        else:
            tx_result = []
            account_name = get_name_from_account_code(account_code)
            if not account_id:
                print("Source account id not found")
                return []

            account_move_lines = odoo_server.SearchRead(
                "account.move.line",
                domain=[("account_id", "=", account_id)],
                order="id asc",
            )
            move_ids = set(
                line["move_id"][0] for line in account_move_lines if "move_id" in line
            )
            account_moves = odoo_server.SearchRead(
                "account.move", domain=[("id", "in", list(move_ids))], order="id asc"
            )

            fields_to_show = ["id", "date", "name", "ref", "amount_total", "state"]

            if account_moves and len(account_moves) > 0:
                for move in account_moves:
                    tx_original = {
                        field: (
                            move[field]
                            if field in move and move[field] is not False
                            else ""
                        )
                        for field in fields_to_show
                    }
                    account_move_lines = odoo_server.SearchRead(
                        "account.move.line",
                        domain=[("id", "in", list(move["line_ids"]))],
                        order="id asc",
                        fields=["account_id", "debit", "credit"],
                    )

                    if len(account_move_lines) == 2:
                        first_account_code = account_move_lines[0]["account_id"][
                            1
                        ].split(" ")[0]
                        second_account_code = account_move_lines[1]["account_id"][
                            1
                        ].split(" ")[0]
                        if account_move_lines[0]["credit"] > 0:
                            sender_account_code = first_account_code
                            receiver_account_code = second_account_code
                        else:
                            receiver_account_code = first_account_code
                            sender_account_code = second_account_code

                        tx = tx_original.copy()
                        tx["positive"] = sender_account_code != account_code
                        tx["sender"] = get_name_from_account_code(sender_account_code)
                        tx["receiver"] = get_name_from_account_code(
                            receiver_account_code
                        )
                        tx_result.append(tx)
                    else:
                        tx_accounts = defaultdict(int)
                        for move_line in account_move_lines:
                            line_account_code = move_line["account_id"][1].split(" ")[0]
                            tx_accounts[line_account_code] += move_line["debit"]
                            tx_accounts[line_account_code] -= move_line["credit"]

                        if tx_accounts[account_code] < 0:  # Account is the sender
                            debiters = []
                            for account, amount in tx_accounts.items():
                                if amount > 0:
                                    debiters.append(account)
                            if len(debiters) == 1:
                                for account, amount in tx_accounts.items():
                                    if account == account_code:
                                        tx = tx_original.copy()
                                        tx["positive"] = False
                                        tx["sender"] = account_name
                                        tx["receiver"] = get_name_from_account_code(
                                            debiters[0]
                                        )
                                        tx["amount_total"] = -1 * amount
                                        tx_result.append(tx)
                            else:
                                tx = tx_original.copy()
                                tx["positive"] = False
                                tx["sender"] = account_name
                                tx["receiver"] = ""
                                tx["amount_total"] = -1 * tx_accounts[account_code]
                                tx_result.append(tx)
                        else:

                            crediters = []
                            for account, amount in tx_accounts.items():
                                if amount < 0:
                                    crediters.append(account)
                            tx = tx_original.copy()
                            tx["positive"] = True
                            tx["receiver"] = account_name
                            if len(crediters) == 1:
                                tx["sender"] = get_name_from_account_code(crediters[0])
                            else:
                                tx["sender"] = ""
                            tx["amount_total"] = tx_accounts[account_code]
                            tx_result.append(tx)
            if settings.CACHE_TIME:
                print(
                    f"Setting cached get_transactions_{account_code}_{count_account_move_lines}"
                )
                cache.set(
                    f"get_transactions_{account_code}_{count_account_move_lines}",
                    tx_result,
                    settings.CACHE_TIME,
                )

    tx_result.reverse()  # We reverse to show the most recent on top
    return tx_result


def get_odoo_user_from_employee(employee_id):
    user_record = odoo_server.SearchRead(
        "hr.employee", [("id", "=", employee_id)], ["user_id"]
    )
    print("get_odoo_user_from_employee", employee_id, "=", user_record)
    if user_record:
        return user_record[0]["user_id"][0]
    else:
        return None


def get_employee_id(odoo_user_id):
    user_record = odoo_server.SearchRead(
        "res.users", [("id", "=", odoo_user_id)], ["employee_id"]
    )
    print("user_record", user_record)
    if user_record[0]["employee_id"]:
        employee_id = user_record[0]["employee_id"][0]
        return employee_id
    else:
        return None


def get_partner_id(odoo_user_id):
    user_record = odoo_server.SearchRead(
        "res.users", [("id", "=", odoo_user_id)], ["partner_id"]
    )
    if user_record[0]["partner_id"]:
        partner_id = user_record[0]["partner_id"][0]
        return partner_id
    else:
        return None


def get_payment_journals():
    journals = odoo_server.SearchRead(
        "account.journal", [("type", "in", ["cash", "bank"])], fields=["id", "name"]
    )
    return journals


def get_liquidity_accounts():
    code_prefix = "572"
    accounts = odoo_server.SearchRead(
        "account.account",
        [("code", "like", f"{code_prefix}%")],
        ["code"],
        order="code desc",
    )

    return accounts


def get_name_from_account_code(account_code):
    from gnb.models import Project, CashAccount

    # print("Getting gnb name for Odoo account code:", account_code)
    if account_code == str(settings.ODOO_ACCOUNT_EXPENSES_PAYMENT):
        return "Transferencia externa"
    if account_code == str(settings.ODOO_ACCOUNT_CLIENTS):
        return "Clientes"

    user_emails = User.objects.filter(
        odooprofile__account_checking=str(account_code)
    ).values_list("email", flat=True)
    if user_emails:
        return user_emails[0]

    project_codes = Project.objects.filter(
        account_checking=str(account_code)
    ).values_list("code", flat=True)
    if project_codes:
        return project_codes[0]

    cashaccounts = CashAccount.objects.filter(
        account_number=str(account_code)
    ).values_list("name", flat=True)
    if cashaccounts:
        return cashaccounts[0]

    return account_code


def get_odoo_account_code(name):
    from gnb.models import Project

    # print("Getting Odoo account code for", name)
    if isinstance(name, int):
        name = str(name)

    if "@" in name:
        # it is a user
        user = User.objects.filter(email=name).first()
        if user:
            return user.odooprofile.account_checking
        else:
            return None
    else:
        # it is a project
        project = Project.objects.filter(code=name).first()
        if project:
            return project.account_checking
        else:
            return None


def create_odoo_account(account):
    code_prefix = account["code_prefix"]
    while True:
        results = odoo_server.SearchRead(
            "account.account",
            [("code", "like", f"{code_prefix}%")],
            ["code"],
            order="code desc",
        )
        code_list = [result["code"] for result in results]
        highest_code = max(
            int(account)
            for account in code_list
            if str(account).startswith(code_prefix)
        )
        print("highest_code", highest_code)

        new_code = highest_code + 1
        code_id = odoo_server.SearchRead(
            "account.account",
            [("code", "=", str(new_code))],
            ["id"],
            order="code desc",
        )
        if not code_id:
            break
    result = odoo_server.SearchRead(
        "account.account", [("code", "=", highest_code)], ["group_id"]
    )
    group_id = result[0]["group_id"][0]
    external_identifier = account["user_type_id_ref"]
    record_data = odoo_server.SearchRead(
        "ir.model.data",
        [
            ("module", "=", external_identifier.split(".")[0]),
            ("name", "=", external_identifier.split(".")[1]),
        ],
        ["res_id"],
        limit=1,
    )
    user_type_id = record_data[0]["res_id"] if record_data else False
    print("Record ID for {}: {}".format(external_identifier, user_type_id))

    account_data = {
        "name": account["name"],
        "code": new_code,
        "user_type_id": user_type_id,
        "group_id": group_id,
    }

    account_id = odoo_server.Create("account.account", [account_data])
    if not account_id:
        return False, False
    else:
        account_id = account_id[0]
        print("new account created", account_id, "with code", new_code)
        return account_id, new_code


def create_cashin_account_move(
    invoice, journal_id, move_date, project_code, amount, pay_taxes
):
    amount_untaxed = invoice["amount_untaxed"]
    amount_total = invoice["amount_total"]
    print("tipo_amount_total", type(amount_total))
    amount_taxes = amount_total - amount_untaxed
    if pay_taxes:
        amount_total = amount
        amount_untaxed = amount - amount_taxes
    else:
        amount_total = amount
        amount_untaxed = amount

    MAIN_PROJECT_CHECKING_ACCOUNT_ID = get_account_id_from_code(
        get_odoo_account_code(get_main_project_code())
    )
    invoice_id = invoice["id"]
    partner_id = invoice["partner_id"][0]
    invoice_name = invoice["name"]
    print("project_code", project_code)
    project_account_id = get_account_id_from_code(get_odoo_account_code(project_code))
    print("move_date", move_date)
    print("invoice_id", invoice_id)
    print("pay_taxes", pay_taxes)
    print("project_account_id", project_account_id)
    print("partner_id", partner_id)
    journal_id = int(journal_id)
    print("journal_id", journal_id)
    CLIENTS_ACCOUNT_ID = get_account_id_from_code(settings.ODOO_ACCOUNT_CLIENTS)
    print("MAIN_PROJECT_CHECKING_ACCOUNT_ID", MAIN_PROJECT_CHECKING_ACCOUNT_ID)
    print("CLIENTS_ACCOUNT_ID", CLIENTS_ACCOUNT_ID)
    line_ids = [
        (
            0,
            0,
            {
                "name": f"{invoice_name}",
                "partner_id": partner_id,
                "account_id": project_account_id,
                "debit": amount_untaxed,
            },
        )
    ]
    if pay_taxes and amount_taxes != 0:
        line_ids.append(
            (
                0,
                0,
                {
                    "name": f"{invoice_name}",
                    "partner_id": partner_id,
                    "account_id": MAIN_PROJECT_CHECKING_ACCOUNT_ID,
                    "debit": amount_taxes,
                },
            )
        )

    line_ids.append(
        (
            0,
            0,
            {
                "name": f"Pago de cliente: {invoice_name}",
                "partner_id": partner_id,
                "account_id": CLIENTS_ACCOUNT_ID,
                "credit": amount_total,
            },
        )
    )

    move_data = {
        "journal_id": journal_id,  # Reemplaza con el ID real del diario
        "ref": invoice["name"],
        "date": move_date,
        "line_ids": line_ids,
    }
    pprint(move_data)

    account_move_id = odoo_server.Create("account.move", [move_data])
    if not account_move_id:
        print("Error creating account_move_id in Odoo Server")
        return None

    print("account_move_id", account_move_id)
    print("Movimiento creado")
    odoo_server.Method("account.move", "post", account_move_id)
    account_move = odoo_server.Read("account.move", account_move_id)
    # The last move line (the credit one) is the one that needs to be assigned
    credit_line_id = account_move[0]["invoice_line_ids"][-1]
    print("credit_line_id", credit_line_id)
    odoo_server.Method(
        "account.move", "js_assign_outstanding_line", invoice_id, credit_line_id
    )

    return account_move_id


def create_bank_account_move(
    account_debit, account_credit, amount, reference, journal_id=None, move_date=None
):
    print("account_debit", account_debit)
    account_debit_id = odoo_server.Search(
        "account.account", [("code", "=", account_debit)], limit=1
    )
    account_debit_id = account_debit_id[0] if account_debit_id else False
    if not account_debit_id:
        return None, "account_debit not found"

    account_credit_id = odoo_server.Search(
        "account.account", [("code", "=", account_credit)], limit=1
    )
    account_credit_id = account_credit_id[0] if account_credit_id else False
    if not account_credit_id:
        return None, "account_credit not found"

    # TODO Check if debit account has enough balance

    print("Source id", account_debit_id)
    print("Destination id", account_credit_id)

    if journal_id is None:
        journal_id = settings.ODOO_JOURNAL_ID_FOR_INTERNAL_TRANSFERENCES
    move_data = {
        "journal_id": journal_id,  # Reemplaza con el ID real del diario
        "ref": reference,
        "line_ids": [
            (
                0,
                0,
                {
                    "account_id": account_debit_id,
                    "debit": amount,
                },
            ),
            (
                0,
                0,
                {
                    "account_id": account_credit_id,
                    "credit": amount,
                },
            ),
        ],
    }
    if move_date:
        move_data["date"] = move_date

    account_move_id = odoo_server.Create("account.move", [move_data])
    if not account_move_id:
        print("Error creating account_move_id in Odoo Server")
        return None, "Error creating account_move_id in Odoo Server"

    print("account_move_id", account_move_id)
    print("Movimiento creado")
    odoo_server.Method("account.move", "post", account_move_id)

    return account_move_id, None


def create_user(user, user_is_member=True):
    email = user.email
    name = user.first_name + " " + user.last_name
    name = name.upper()
    print("CREANDO usuario en ODOO:", email, name)
    details = {}
    result_ids = odoo_server.Search("res.users", [("login", "=", user.email)])
    if result_ids:
        odoo_user_id = result_ids[0]
    else:
        print(f"Odoo: No result found with: {email}. Creating...")
        user_data = {
            "name": name,  # Full name of the user
            "login": email,  # Username
            "password": utils.generate_password(
                12
            ),  # Random Password. User's are not supossed to login in Odoo
            "email": email,
            "groups_id": [(6, 0, [settings.ODOO_DEFAULT_GROUP_NEW_USERS])],
        }
        print("user_data", user_data)
        odoo_user_id = odoo_server.Create("res.users", [user_data])
        odoo_user_id = odoo_user_id[0]
        details["odoo_user_id"] = odoo_user_id

        user_record = odoo_server.SearchRead(
            "res.users", [("id", "=", odoo_user_id)], ["partner_id"]
        )
        if user_record:
            # Disabling email notifications in odoo for the user
            odoo_partner_id = user_record[0]["partner_id"][0]
            odoo_server.Write("res.partner", [odoo_partner_id], {"email": False})

    odoo_profile, created = OdooProfile.objects.get_or_create(
        user=user,
        defaults={
            "odoo_user_id": odoo_user_id,
        },
    )

    details["django_odoo_profile_created"] = created

    if not odoo_profile.account_checking:
        checking_account = {
            "name": f"C.P. {name}",
            "code_prefix": "572",  # TODO: This should be better using Odoo Account Groups
            "user_type_id_ref": "account.data_account_type_liquidity",
        }

        checking_account_id, account_code = create_odoo_account(checking_account)
        odoo_profile.account_checking = account_code
        details["account_checking"] = account_code
    else:
        checking_account_id = get_account_id_from_code(odoo_profile.account_checking)

    # BANK JOURNAL
    bank_journal_code = f"C.P. {name}"
    print("bank_journal_code", bank_journal_code)

    # TODO: USE UNIQUE CODES, no NAMES, like EMAILS
    journal_ids = odoo_server.Search(
        "account.journal", [("name", "=", bank_journal_code)]
    )
    if not journal_ids:
        print(f"No journal found with code: {bank_journal_code}. Creating...")
        # TODO: get res_partner_bank and res_bank
        bank_journal_data = {
            "name": f"C.P. {name}",
            "code": bank_journal_code,
            "type": "bank",
            "default_debit_account_id": checking_account_id,
            "default_credit_account_id": checking_account_id,
            # "bank_account_id": #TODO falta añadir la 1 de res.partner.bank
        }
        bank_journal_id = odoo_server.Create("account.journal", [bank_journal_data])
        details["bank_journal_id"] = bank_journal_id[0]

    if user_is_member:
        if not odoo_profile.account_voluntary_contribution:
            account_voluntary_contribution = {
                "name": f"Aportación Voluntaria {name.upper()}",
                "code_prefix": "1001",
                "user_type_id_ref": "account.data_account_type_non_current_liabilities",  # Pasivos no-circulantes
            }
            _, account_code = create_odoo_account(account_voluntary_contribution)
            odoo_profile.account_voluntary_contribution = account_code
            details["account_voluntary_contribution"] = account_code

        if not odoo_profile.account_social_capital:
            account_social_capital = {
                "name": f"Capital Social {name.upper()}",
                "code_prefix": "1000",
                "user_type_id_ref": "account.data_account_type_non_current_liabilities",  # Pasivos no-circulantes
            }
            _, account_code = create_odoo_account(account_social_capital)
            odoo_profile.account_social_capital = account_code
            details["account_social_capital"] = account_code

        if not odoo_profile.account_loans:
            account_loans = {
                "name": f"PRESTAMO {name.upper()}",
                "code_prefix": "5324",
                "user_type_id_ref": "account.data_account_type_current_assets",  # Activos circulantes
            }
            _, account_code = create_odoo_account(account_loans)
            odoo_profile.account_loans = account_code
            details["account_loans"] = account_code

    odoo_profile.save(
        update_fields=(
            "odoo_user_id",
            "account_checking",
            "account_social_capital",
            "account_loans",
            "account_voluntary_contribution",
        )
    )

    return details


def create_project(project, project_is_member=True):
    code = project.code
    name = project.name
    details = {}
    if not project.account_checking:
        checking_account = {
            "name": f"C.C.{code} {name}",
            "code_prefix": "572",
            "user_type_id_ref": "account.data_account_type_liquidity",
        }
        checking_account_id, checking_account_code = create_odoo_account(
            checking_account
        )
        project.account_checking = checking_account_code
        details["checking_account_code"] = checking_account_code
    else:
        checking_account_id = get_account_id_from_code(project.account_checking)

    # BANK JOURNAL
    bank_journal_code = f"C.C.{code} {name}"
    journal_ids = odoo_server.Search(
        "account.journal", [("code", "=", bank_journal_code)]
    )
    if not journal_ids:
        print(f"No journal found with code: {bank_journal_code}. Creating...")
        # res_partner_bank = TODO: get res.partner.bnak settings.MAIN_RES_PARTNER_BANK_ODOO_ID
        # res_bank = res_partner_bank["bank_id"]
        bank_journal_data = {
            "name": f"C.C.{code} {name}",
            "code": bank_journal_code,
            "type": "bank",
            # "bank_id": res_bank,
            # "bank_account_id": #TODO falta añadir la 1 de res.partner.bank
            "default_debit_account_id": checking_account_id,
            "default_credit_account_id": checking_account_id,
        }
        bank_journal_id = odoo_server.Create("account.journal", [bank_journal_data])
        details["bank_journal_id"] = bank_journal_id[0]

    if project_is_member:
        # SALES ACCOUNT
        if not project.account_sales:
            sales_account = {
                "name": f"INGRESOS.{code} {name}",
                "code_prefix": "705",
                "user_type_id_ref": "account.data_account_type_revenue",
            }

            sales_account_id, sales_account_code = create_odoo_account(sales_account)
            print("sales_account_id created", sales_account_id)
            print("sales_account_code created", sales_account_code)
            project.account_sales = sales_account_code
            details["sales_account_code"] = sales_account_code
        else:
            sales_account_id = get_account_id_from_code(project.account_sales)

        # SALES JOURNAL
        sales_journal_code = f"{code}"
        journal_ids = odoo_server.Search(
            "account.journal", [("code", "=", sales_journal_code)]
        )
        if not journal_ids:
            print(f"No journal found with code: {sales_journal_code}. Creating...")
            sales_journal_data = {
                "name": f"INGRESOS.{code} {name}",
                "code": sales_journal_code,
                "type": "sale",
                "default_debit_account_id": sales_account_id,
                "default_credit_account_id": sales_account_id,
            }
            sales_journal_id = odoo_server.Create(
                "account.journal", [sales_journal_data]
            )
            details["sales_journal_id"] = sales_journal_id[0]

        # EXPENSES ACCOUNT
        if not project.account_expenses:
            expense_account = {
                "name": f"GASTOS.{code} {name}",
                "code_prefix": "620",
                "user_type_id_ref": "account.data_account_type_expenses",
            }

            expenses_account_id, expenses_account_code = create_odoo_account(
                expense_account
            )
            project.account_expenses = expenses_account_code
            details["expenses_account_code"] = expenses_account_code
        else:
            expenses_account_id = get_account_id_from_code(project.account_expenses)

        # EXPENSES JOURNAL
        expenses_journal_code = f"G.{code}"
        journal_ids = odoo_server.Search(
            "account.journal", [("code", "=", expenses_journal_code)]
        )
        if not journal_ids:
            print(f"No journal found with code: {expenses_journal_code}. Creating...")
            expense_journal_data = {
                "name": f"GASTOS.{code} {name}",
                "code": expenses_journal_code,
                "type": "purchase",
                "default_debit_account_id": expenses_account_id,
                "default_credit_account_id": expenses_account_id,
            }
            expense_journal_id = odoo_server.Create(
                "account.journal", [expense_journal_data]
            )
            details["expense_journal_id"] = expense_journal_id[0]

        if not project.account_loans:
            # 171xxxx prefix is for long term loans(more than current year)
            # 5324xxx are for current year loans
            loans_account = {
                "name": f"PRESTAMO.{code} {name}",
                "code_prefix": "5324",
                "user_type_id_ref": "account.data_account_type_current_assets",
            }

            loans_account_id, loans_account_code = create_odoo_account(loans_account)
            project.account_loans = loans_account_code
            details["loans_account_code"] = loans_account_code

        product_default_code = f"{code}-01"
        product_ids = odoo_server.Search(
            "product.product", [("default_code", "=", product_default_code)]
        )
        if not product_ids:
            print(f"No product found with code: {product_default_code}. Creating...")
            product_data = {
                "default_code": product_default_code,
                "name": f"SERVICIO {name}",
                "property_account_income_id": sales_account_id,
                "property_account_expense_id": expenses_account_id,
            }
            product_id = odoo_server.Create("product.product", [product_data])
            print("Product id created:", product_id)
            details["product_id"] = product_id[0]

        project.save(
            update_fields=(
                "account_checking",
                "account_expenses",
                "account_sales",
                "account_loans",
            )
        )

    return details
