from django.urls import path
from . import views

app_name = "odoo"  # Opcional, pero útil para evitar conflictos de nombres

urlpatterns = [
    path(
        "tax_id/<str:tax_id_number>/", views.get_tax_id_number, name="get_tax_id_number"
    ),
    path("invoice/", views.InvoiceView.as_view(), name="invoice"),
    path(
        "invoice/validate/<int:invoice_id>/",
        views.validate_invoice,
        name="validate_invoice",
    ),
    path("expense/", views.ExpenseView.as_view(), name="expense"),
    path(
        "expense/validate/<int:expense_sheet_id>/",
        views.validate_expense_sheet,
        name="validate_expense",
    ),
    path("account_move/", views.BankAccountMoveView.as_view(), name="account_move"),
    path(
        "cash_account_move/",
        views.CashAccountMoveView.as_view(),
        name="cash_account_move",
    ),
    path(
        "delete_account_move/",
        views.delete_account_move,
        name="delete_account_move",
    ),
]
