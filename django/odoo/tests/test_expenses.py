from django.test import SimpleTestCase
from unittest.mock import patch
import datetime
from odoo.expenses import *


class CreateExpenseAccountMoveTest(SimpleTestCase):

    def setUp(self):
        self.expense_sheet = {
            "id": 1,
            "total_amount": 100,
            "taxed_amount": 10,
            "untaxed_amount": 90,
            "name": "Test ExpenseSheet",
            "reference": "R001"
        }
        self.move_date = datetime.datetime.now().date().strftime("%Y-%m-%d")
        self.project_code = "PRO_EJE"

    @patch('odoo.accounts')
    @patch('odoo.expenses.odoo_server')
    def test_create_expense_account_move_success(self, mock_odoo_server, mock_accounts):
        mock_accounts.get_account_id_from_code.return_value = 1
        mock_accounts.get_main_project_code.return_value = "PRO_EJE"
        mock_accounts.get_odoo_account_code.return_value = 10123
        mock_odoo_server.Create.return_value = 123

        result, error = create_expense_account_move(self.expense_sheet, self.move_date, self.project_code)

        self.assertEqual(result, 123)
        self.assertIsNone(error)
        # Check Create and Method functions are called
        mock_odoo_server.Create.assert_called_once()
        mock_odoo_server.Method.assert_called_once_with("account.move", "post", 123)


    @patch('odoo.accounts')
    @patch('odoo.expenses.odoo_server')
    def test_create_expense_account_move_error(self, mock_odoo_server, mock_accounts):
        mock_accounts.get_account_id_from_code.return_value = 1
        mock_accounts.get_main_project_code.return_value = "PRO_EJE"
        mock_accounts.get_odoo_account_code.return_value = 10123
        mock_odoo_server.Create.return_value = None

        result = create_expense_account_move(self.expense_sheet, self.move_date, self.project_code)

        self.assertIsNone(result)
        # Check Method function is not called when Create throws an error
        mock_odoo_server.Create.assert_called_once()
        mock_odoo_server.Method.assert_not_called()


class CreateOdooExpenseTest(SimpleTestCase):

    def setUp(self):
        self.data = {
            "journal": "PRO_EJE",
            "odoo_employee_id": 100,
            "date": "2024-01-01",
            "lines": [
                {
                    "reference": "Ref1",
                    "description": "Desc1",
                    "price_unit": 100.0,
                    "quantity": 1,
                    "date": "2024-03-17",
                },
            ],
            "type_expense": "normal_expense",
        }

    @patch('odoo.expenses.odoo_server')
    def test_create_odoo_expense_success(self, mock_odoo_server):
        mock_odoo_server.SearchRead.side_effect = [
            [{"id": 1, "sequence_number_next": 100}],  # Return for account.journal call
            [{"id": 200, "property_account_expense_id": [1]}]  # Return for product.product call
        ]
        mock_odoo_server.Create.side_effect = [
            [500],  # Return for hr.expense call
            [501]  # Return for hr.expense.sheet call
        ]

        result, error_message = create_odoo_expense(self.data)

        self.assertIsNotNone(result)
        self.assertFalse(error_message)
        # Check expense and expense sheet are created
        self.assertEqual(mock_odoo_server.Create.call_count, 2)

    @patch('odoo.expenses.odoo_server')
    def test_create_odoo_expense_no_journal(self, mock_odoo_server):
        mock_odoo_server.SearchRead.return_value = []

        result, error_message = create_odoo_expense(self.data)

        self.assertIsNone(result)
        self.assertEqual(error_message, "ODOO_JOURNAL_NOT_FOUND_ERROR")
        # Check expense is not created
        mock_odoo_server.Create.assert_not_called()

    @patch('odoo.expenses.odoo_server')
    def test_create_odoo_expense_no_product(self, mock_odoo_server):
        mock_odoo_server.SearchRead.side_effect = [
            [{"id": 1, "sequence_number_next": 100}],  # Return account.journal call
            [{}]  # Return product.product call
        ]
        #TO DO: Error message when product not found

    @patch('odoo.expenses.odoo_server')
    def test_create_odoo_expense_error_creating_expense(self, mock_odoo_server):
        mock_odoo_server.SearchRead.side_effect = [
            [{"id": 1, "sequence_number_next": 100}],  # Return account.journal call
            [{"id": 200, "property_account_expense_id": [1]}]  # Return product.product call
        ]
        mock_odoo_server.Create.side_effect = Exception("Error creating expense")

        result, error_message = create_odoo_expense(self.data)

        self.assertIsNone(result)
        self.assertEqual(error_message, "ODOO_CREATING_EXPENSE_ERROR")
        self.assertEqual(mock_odoo_server.SearchRead.call_count, 2)
        # Check expense sheet is not created, only one call for hr.expense
        self.assertEqual(mock_odoo_server.Create.call_count, 1)


    @patch('odoo.expenses.odoo_server')
    def test_create_odoo_expense_error_creating_expense_sheet(self, mock_odoo_server):
        mock_odoo_server.SearchRead.side_effect = [
            [{"id": 1, "sequence_number_next": 100}],  # Return account.journal call
            [{"id": 200, "property_account_expense_id": [1]}]  # Return product.product call
        ]
        mock_odoo_server.Create.side_effect = [
            [500],
            Exception("Error creating expense sheet")
        ]

        result, error_message = create_odoo_expense(self.data)

        self.assertIsNone(result)
        self.assertEqual(error_message, "ODOO_CREATING_EXPENSE_SHEET_ERROR")
        self.assertEqual(mock_odoo_server.SearchRead.call_count, 2)
        # Check expense is created and another call for expense sheet Create error
        self.assertEqual(mock_odoo_server.Create.call_count, 2)


class GetExpenseSheetTest(SimpleTestCase):

    @patch('odoo.expenses.odoo_server')
    def test_get_expense_sheet_success(self, mock_odoo_server):
        mock_odoo_server.Read.side_effect = [
            #hr.expense.sheet call
            [{
                "id": 1,
                "expense_line_ids": [101, 102],
                "total_amount": 80.0
            }],
            #hr.expense calls
            [{
                "id": 101,
                "total_amount": 50.0,
                "untaxed_amount": 30.0
            }],
            [{
                "id": 102,
                "total_amount": 30.0,
                "untaxed_amount": 20.0
            }]
        ]

        result_expense_sheet, result_move_lines = get_expense_sheet(1)

        self.assertIsNotNone(result_expense_sheet)
        self.assertIsNotNone(result_move_lines)
        self.assertEqual(len(result_move_lines), 2)
        self.assertAlmostEqual(result_expense_sheet["total_amount"], 80.0)
        self.assertAlmostEqual(result_expense_sheet["taxed_amount"], 30.0)
        self.assertAlmostEqual(result_expense_sheet["untaxed_amount"], 50.0)
        #Check 3 Read call: one for expense sheet and one for each expense
        self.assertEqual(mock_odoo_server.Read.call_count, 3)

    @patch('odoo.expenses.odoo_server')
    def test_get_expense_sheet_not_found(self, mock_odoo_server):
        mock_odoo_server.Read.return_value = []

        result = get_expense_sheet(1)

        self.assertIsNone(result)

    @patch('odoo.expenses.odoo_server')
    def test_get_expense_sheet_success_no_expenses(self, mock_odoo_server):
        mock_odoo_server.Read.side_effect = [
            #hr.expense.sheet call
            [{
                "id": 1,
                "expense_line_ids": [],
                "total_amount": 80.0
            }],
            #hr.expense calls
            []
        ]

        result_expense_sheet, result_move_lines = get_expense_sheet(1)

        self.assertIsNotNone(result_expense_sheet)
        self.assertIsNotNone(result_move_lines)
        self.assertEqual(len(result_move_lines), 0)
        self.assertAlmostEqual(result_expense_sheet["total_amount"], 80.0)
        self.assertAlmostEqual(result_expense_sheet["taxed_amount"], 0.0)
        self.assertAlmostEqual(result_expense_sheet["untaxed_amount"], 80.0)
        # Check only one Read call because 0 expense line ids
        self.assertEqual(mock_odoo_server.Read.call_count, 1)


class GetExpensesIdsAllowed(SimpleTestCase):

    @patch('odoo.accounts')
    @patch('odoo.expenses.odoo_server')
    def test_get_expenses_ids_allowed(self, mock_odoo_server, mock_accounts):
        mock_accounts.get_employee_id.return_value = 100
        mock_odoo_server.Search.return_value = [201,202]

        result = get_expenses_ids_allowed(100)

        self.assertEqual(result, [201,202])
        self.assertEqual(mock_odoo_server.Search.call_count, 1)


class TestGetExpenses(SimpleTestCase):

    @patch('odoo.expenses.odoo_server')
    def test_get_expenses_with_employee_id(self, mock_odoo_server):
        user_id = 1
        employee_id = 10  # Employee ID associated with user id
        mock_odoo_server.SearchRead.side_effect = [
            [{"employee_id": [employee_id]}], # call to get user_record from res.users
            # hr.expense.sheet call
            [
                {"id": 1, "name": "Expense Sheet 1", "employee_id": employee_id, "accounting_date": "2022-01-01", "state": "draft", "total_amount": 100.0},
                {"id": 2, "name": "Expense Sheet 2", "employee_id": employee_id, "accounting_date": "2022-02-01", "state": "posted", "total_amount": 150.0}
            ],
        ]

        result = get_expenses(user_id)

        self.assertEqual(len(result), 2)
        self.assertEqual(result[0]["name"], "Expense Sheet 1")
        self.assertEqual(result[1]["employee_id"], employee_id)
        self.assertEqual(mock_odoo_server.SearchRead.call_count, 2)

    @patch('odoo.expenses.odoo_server')
    def test_get_expenses_without_employee_id(self, mock_odoo_server):
        user_id = 1
        employee_id = 10  # Employee ID associated with user id
        mock_odoo_server.SearchRead.return_value = []

        result = get_expenses(user_id)

        self.assertFalse(result)
        self.assertEqual(mock_odoo_server.SearchRead.call_count, 1)

    @patch('odoo.expenses.odoo_server')
    def test_get_expenses_without_expenses(self, mock_odoo_server):
        user_id = 1
        employee_id = 10  # Employee ID associated with user id
        mock_odoo_server.SearchRead.side_effect = [
            [{"employee_id": [employee_id]}], # call to get user_record from res.users
            [], # hr.expense.sheet call
        ]

        result = get_expenses(user_id)

        self.assertEqual(result, [])
        self.assertEqual(mock_odoo_server.SearchRead.call_count, 2)

