import pprint
import string
import secrets


def generate_password(length=12):
    characters = string.ascii_letters + string.digits + string.punctuation
    password = (
        secrets.choice(string.ascii_lowercase)
        + secrets.choice(string.ascii_uppercase)
        + secrets.choice(string.digits)
        + secrets.choice(string.punctuation)
    )
    password += "".join(secrets.choice(characters) for i in range(length - 4))
    password_list = list(password)
    secrets.SystemRandom().shuffle(password_list)
    return "".join(password_list)


from odooclient import tools

from django.utils.text import slugify
from django.contrib.auth.models import User
from odoo.models import OdooProfile
from odoo import odoo_server
from gnb.models import Project, ProjectMembership

csv_file_path_users = "/tmp/Empleados-Usuarios-GNB.csv"
csv_file_path_projects = "/tmp/PROYECTOS-CUENTAS-GNB.csv"

IMPORT_USERS = True
IMPORT_PROJECTS = True
CREATE_GNB_FOR_USER = True
START_FROM_ROW = 0

from django.core.management.base import BaseCommand

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = (
        "Import script to populate from two csv files the django user/project database"
    )

    def handle(self, *args, **kwargs):

        if IMPORT_USERS:
            data_to_create_profiles = tools.read_csv_data(
                csv_file_path_users, delimiter=";"
            )
            # Deleting previous users
            User.objects.all().delete()
            Project.objects.all().delete()
            for i, data in enumerate(data_to_create_profiles):
                if i < START_FROM_ROW:
                    print(f"Saltando fila {i}...")
                    continue
                pp.pprint(data)
                odoo_user_id = None
                odoo_partner_id = None

                archivado = (
                    True if data["ARCHIVADO"] and data["ARCHIVADO"] == "TRUE" else False
                )
                if archivado:
                    print(f"Ignorando fila {i} archivado...")
                    continue

                odoo_user_id = (
                    int(data["user_id"])
                    if data["user_id"] and data["user_id"] != "FALSO"
                    else None
                )
                odoo_partner_id = (
                    int(data["odoo_partner_id"])
                    if data["odoo_partner_id"] and data["odoo_partner_id"] != "FALSO"
                    else None
                )
                employee_id = (
                    int(data["employee_id"])
                    if data["employee_id"] and data["employee_id"] != "FALSO"
                    else None
                )
                nombre = data["Nombre"]
                nombre_completo = data["Nombre Completo"]
                if not nombre:
                    print(f"Falló fila {i} sin nombre...")
                    b
                email = (
                    data["EMAIL"]
                    if "@" in data["EMAIL"]
                    else slugify(nombre).lower() + "@estraperlo.org"
                )
                existing_user_ids = odoo_server.Search(
                    "res.users", [("login", "=", email)]
                )

                if not odoo_user_id and not employee_id:
                    if existing_user_ids:
                        # The user with the given email already exists
                        odoo_user_id = existing_user_ids[0]
                        print(
                            f"User with email '{email}' already exists in Odoo. User ID: {odoo_user_id}"
                        )
                    else:
                        print(f"CREANDO usuario en ODOO en fila {i}, ")
                        # group_salesman = odoo_server.Search('res.groups', [('name', 'like', 'User: Own Documents Only')])
                        # print('group_salesman', group_salesman)
                        group_user_own_documents_only = 33
                        user_data = {
                            "name": nombre_completo,  # Full name of the user
                            "login": email,  # Username
                            "password": "XXXXXXXXXYYY",  # Password
                            "email": email,  # Email
                            "groups_id": [(6, 0, [group_user_own_documents_only])],
                        }
                        odoo_user_id = odoo_server.Create("res.users", [user_data])
                        odoo_user_id = odoo_user_id[0]
                        print(
                            f"New user created with ID: {odoo_user_id} and partner_id: {odoo_partner_id} for name: {nombre}"
                        )

                        # 33 # account_group_
                    if odoo_user_id:
                        user_record = odoo_server.SearchRead(
                            "res.users", [("id", "=", odoo_user_id)], ["partner_id"]
                        )
                        if user_record:
                            odoo_partner_id = user_record[0]["partner_id"][0]
                            odoo_server.Write(
                                "res.partner", [odoo_partner_id], {"email": False}
                            )

                if CREATE_GNB_FOR_USER:
                    password = generate_password(14)
                    try:
                        account_checking = (
                            int(data["CUENTA PERSONAL"])
                            if data["CUENTA PERSONAL"]
                            else None
                        )
                    except:
                        continue
                    account_social_capital = (
                        int(data["CUENTA CAPITAL SOCIAL"])
                        if data["CUENTA CAPITAL SOCIAL"]
                        else None
                    )
                    account_voluntary_contribution = (
                        int(data["CUENTA APORTACIONES VOLUNTARIAS"])
                        if data["CUENTA APORTACIONES VOLUNTARIAS"]
                        else None
                    )

                    print(
                        f"Registrando usuario GNB para {email} con password: {password}"
                    )
                    user, created = User.objects.get_or_create(
                        username=email, defaults={"email": email}
                    )
                    user.set_password(password)

                    user.is_active = True
                    if data["ADMIN"] == "TRUE":
                        user.is_superuser = True
                        user.is_staff = True
                        print("Usuario asignado como admin")

                    membership_number = data["COOP N.º"] if data["COOP N.º"] else None

                    if nombre:
                        nombre = nombre.split(" ")
                        user.first_name = nombre[0]
                        if len(nombre) > 1:
                            user.last_name = " ".join(nombre[1:])
                    user.save()
                    print("odoo_user_id", odoo_user_id)
                    print("odoo_partner_id", odoo_partner_id)
                    if not odoo_user_id:
                        continue
                        # Solve this
                        user_ids = odoo_server.Search(
                            "res.users", [("employee_id", "=", str(employee_id))]
                        )
                        if user_ids:
                            # The user with the given partner ID exists
                            odoo_user_id = user_ids[0]
                    if odoo_user_id and not odoo_partner_id:
                        user_record = odoo_server.SearchRead(
                            "res.users", [("id", "=", odoo_user_id)], ["partner_id"]
                        )
                        if user_record:
                            odoo_partner_id = user_record[0]["partner_id"][0]
                        else:
                            print("not found partner_id for user_id", odoo_user_id)

                    odoo_profile, created = OdooProfile.objects.get_or_create(
                        user=user,
                        defaults={
                            "odoo_user_id": odoo_user_id,
                            #'odoo_partner_id': odoo_partner_id,
                            "account_checking": account_checking,
                            "account_social_capital": account_social_capital,
                            "account_voluntary_contribution": account_voluntary_contribution,
                            "membership_number": membership_number,
                        },
                    )

                    print(f"OdooProfile para odoo_user_id {odoo_user_id}")

                    projects = data.get("PROYECTOS").split("-")
                    for project_code in projects:
                        project_code = project_code.strip()
                        if project_code:
                            print(f'Found project "{project_code}"')
                            project, created = Project.objects.get_or_create(
                                code=project_code
                            )
                            item, created = ProjectMembership.objects.get_or_create(
                                user=user, project=project
                            )
                            item.role = "member"
                            item.save()
                            print(f"Added as member to project {project}")

                    projects = data.get("ADMIN PROYECTO").split("-")
                    for project_code in projects:
                        project_code = project_code.strip()
                        if project_code:
                            project, created = Project.objects.get_or_create(
                                code=project_code
                            )
                            item, created = ProjectMembership.objects.get_or_create(
                                user=user, project=project
                            )
                            item.role = "admin"
                            item.save()
                            print(f"Added as admin to project {project}")

        if IMPORT_PROJECTS:
            data_to_create_projects = tools.read_csv_data(
                csv_file_path_projects, delimiter=";"
            )
            for data in data_to_create_projects:
                pp.pprint(data)
                account_checking = None
                account_expenses = None
                account_sales = None
                project_code = data["PROYECTO"]
                project_name = data["NOMBRE PROYECTO"]
                print(f"Añadiendo o modificando project {project_code}")
                to_create = True if "CREAR" in data and data["CREAR"] != "" else False
                if not to_create:
                    account_checking = (
                        int(data["CUENTA CORRIENTE PROYECTO"])
                        if data["CUENTA CORRIENTE PROYECTO"]
                        else None
                    )
                    account_expenses = int(data["GASTOS"]) if data["GASTOS"] else None
                    account_sales = int(data["INGRESOS"]) if data["INGRESOS"] else None
                else:
                    pass
                    # account_checking, account_expenses, account_sales = odoo.create_project(project_code, project_name)

                project, created = Project.objects.get_or_create(code=project_code)
                project.account_checking = account_checking
                project.account_expenses = account_expenses
                project.account_sales = account_sales
                project.name = project_name

                project.save()
                print(f"Guardados datos del proyecto {project_code}")
