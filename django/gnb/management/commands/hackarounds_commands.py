import pprint
import string
import secrets
from odooclient import tools
from django.utils.text import slugify
from django.contrib.auth.models import User
from odoo.models import OdooProfile
from odoo import odoo_server
from gnb.models import Project, ProjectMembership

from django.core.management.base import BaseCommand

pp = pprint.PrettyPrinter(indent=4)


class Command(BaseCommand):
    help = (
        "Import script to populate from two csv files the django user/project database"
    )

    def handle(self, *args, **kwargs):
        pass
