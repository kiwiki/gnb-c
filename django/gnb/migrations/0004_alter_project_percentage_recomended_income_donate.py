# Generated by Django 4.2.6 on 2024-01-18 17:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gnb', '0003_project_percentage_recomended_income_donate_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='percentage_recomended_income_donate',
            field=models.FloatField(blank=True, default=5, null=True, verbose_name='% de donaciones'),
        ),
    ]
