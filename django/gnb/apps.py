from django.apps import AppConfig

class GNBConfig(AppConfig):
    name = "gnb"
    verbose_name = "Gestión de GNB"  # translatable
