from functools import wraps
from django.http import HttpResponseForbidden
from django.contrib.auth.models import User
from django.shortcuts import render


def check_user_access(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        user_id = request.GET.get("user", None)
        user = request.user

        if not user_id:
            # If empty, get the user logged in's own invoices
            user_id = user.id
        else:  # an admin is logging as that user
            user_id = int(user_id)
            if user.id != user_id and not request.user.is_superuser:
                # If non-admin user chooses a different user id in the params
                return HttpResponseForbidden("Access denied. Event reported to admins.")
            else:
                user = User.objects.get(id=user_id)

        try:
            user.odooprofile
        except:
            context = {
                "odooprofile_error": "This user does not have an Odoo profile. Please contact with an administrator.",
            }
            request = render(request, "registration/login.html", context)
            return request

        return view_func(request, user=user, *args, **kwargs)

    return _wrapped_view
