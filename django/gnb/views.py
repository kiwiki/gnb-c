from colorama import Fore, Back, Style, init
from pprint import pprint

init()

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, reverse
from django.contrib.auth.decorators import login_required
from django.http import (
    HttpResponseForbidden,
    HttpResponseBadRequest,
    HttpResponseNotFound,
)
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.models import User
import odoo.invoices
import odoo.expenses
import odoo.accounts
import gnb.utils as utils
from gnb.models import Project, ProjectMembership
from gnb.decorators import check_user_access
from django.core.mail import send_mail
from django.conf import settings


def add_menu_context(request, context, page):
    context["all_users"] = User.objects.all()
    context["navbar"] = page
    return context


def frontend_view(request):
    if request.user.is_authenticated:

        context = {
            "meta_opengraph": "Metadata info",
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
        }
        context = add_menu_context(request, context, "frontview")
        return redirect("my_dashboard_view")
        # response = render(request, "frontend/portal_logged_in.html", context)
    else:
        context = {"navbar": "home"}
        response = render(request, "frontend/portal_logged_out.html", context)

    return response


@login_required
def contact_view(request):
    user = request.user
    if request.method == "POST":
        contact_msg = request.POST.get("contact_msg")
        user_email = user.email
        subject = "GNB: Formulario de Contacto."
        message = f"""
Hemos recibido tu mensaje vía GNB con el siguiente texto:

{contact_msg}

Email de respuesta: {user_email}
Se responderá lo más brevemente posible."""

        email_to = [user_email] + settings.ADMINS_EMAIL
        try:
            send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, email_to)
            messages.success(request, "Formulario enviado exitosamente.")
        except Exception as e:
            print(f"An error occurred sending mail: {e}")
            messages.error(request, "El correo electrónico no se pudo enviar.")

        return redirect("contact_view")
    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": request.user.id,
    }

    context = add_menu_context(request, context, "contact")
    return render(request, "frontend/contact.html", context)


@check_user_access
@login_required
def get_invoices_view(request, user):
    odoo_user_id = user.odooprofile.odoo_user_id
    items = odoo.invoices.get_invoices_by_user(odoo_user_id)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": user.id,
        "items": items,
    }
    context = add_menu_context(request, context, "get_invoices")
    return render(request, "frontend/get_invoices.html", context)


@check_user_access
@login_required
def get_expense_sheet_view(request, expense_sheet_id, user):
    odoo_user_id = user.odooprofile.odoo_user_id
    if (
        expense_sheet_id not in odoo.expenses.get_expenses_ids_allowed(odoo_user_id)
        and not user.is_superuser
    ):
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    expense_sheet, move_lines = odoo.expenses.get_expense_sheet(expense_sheet_id)
    if not expense_sheet:
        return HttpResponseNotFound("Expense Sheet id not found.")
    else:
        context = {
            "meta_opengraph": "Metadata info",
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
            "user_id": user.id,
            "object": expense_sheet,
            "move_lines": move_lines,
        }
        context = add_menu_context(request, context, "expense")
        return render(request, "frontend/get_expense.html", context)


@check_user_access
@login_required
def get_invoice_view(request, invoice_id, user):
    user_is_project_admin = False
    odoo_user_id = user.odooprofile.odoo_user_id
    invoice, move_lines = odoo.invoices.get_invoice(invoice_id)

    if not invoice:
        return HttpResponseNotFound("Invoice id not found.")

    project_code = odoo.invoices.get_project_from_invoice_name(invoice["name"])
    if project_code:
        p = Project.objects.filter(code=project_code).first()
        if p:
            user_is_project_admin = ProjectMembership.objects.filter(
                user=user, project=p, role="admin"
            ).exists()

    if (
        not user_is_project_admin
        and invoice_id not in odoo.invoices.get_invoices_ids_allowed(odoo_user_id)
        and not user.is_superuser
    ):
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    partner = odoo.invoices.get_partner(invoice["partner_id"][0])
    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": user.id,
        "partner": partner,
        "invoice": invoice,
        "move_lines": move_lines,
        "INVOICE_ORG_DETAILS": getattr(settings, "INVOICE_ORG_DETAILS", ""),
        "INVOICE_ORG_DETAILS_BELOW": getattr(settings, "INVOICE_ORG_DETAILS_BELOW", ""),
    }
    context = add_menu_context(request, context, "invoice")
    return render(request, "frontend/get_invoice.html", context)


@check_user_access
@login_required
def new_invoice_view(request, project_code, user):

    # Returning all the project names except the generic EST
    p = Project.objects.filter(code=project_code).first()
    if not p:
        return HttpResponseForbidden("El proyecto no existe.")
    if p.archived:
        return HttpResponseForbidden("Error. Proyecto archivado.")

    pnames = utils.get_user_projects(user, exclude_main=True).keys()
    if project_code not in pnames:
        return HttpResponseForbidden("Access denied. Event reported to admins.")
    products = odoo.invoices.get_products([project_code])

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "project_code": project_code,
        "user_id": user.id,
        "products": products,
    }
    context = add_menu_context(request, context, "new_invoice")
    return render(request, "frontend/new_invoice.html", context)


@check_user_access
@login_required
def new_expense_view(request, project_code, user):
    # Returning all the project names except the generic EST
    p = Project.objects.filter(code=project_code).first()
    if not p:
        return HttpResponseForbidden("El proyecto no existe.")
    if p.archived:
        return HttpResponseForbidden("Error. Proyecto archivado.")
    pnames = utils.get_user_projects(user, exclude_main=True).keys()
    if project_code not in pnames:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "projects": pnames,
        "project_code": project_code,
        "user_id": user.id,
    }
    context = add_menu_context(request, context, "new_expense")
    return render(request, "frontend/new_expense.html", context)


@check_user_access
@login_required
def get_expenses_view(request, user):

    odoo_user_id = user.odooprofile.odoo_user_id
    items = odoo.expenses.get_expenses(odoo_user_id)
    if items is False:
        return HttpResponseNotFound(
            "Error al encontrar los gastos del usuario. Contacta a l@s administrador@s."
        )

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": user.id,
        "items": items,
    }
    context = add_menu_context(request, context, "get_expenses")
    return render(request, "frontend/get_expenses.html", context)


@login_required
def users_report_view(request, user_id=None):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    total_users = []
    users = User.objects.filter(odooprofile__account_checking__isnull=False)
    for i, user in enumerate(users):
        print(f"Calculando balances de usuario {user.username} ({i}/{len(users)})")
        data_user = {"username": user.username}
        data_user["account_checking"] = odoo.accounts.get_total_balances(
            user.odooprofile.account_checking, "bank"
        )
        data_user["account_social_capital"] = odoo.accounts.get_total_balances(
            user.odooprofile.account_social_capital
        )
        total_users.append(data_user)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "reports",
        "total_users": total_users,
    }
    context = add_menu_context(request, context, "reports")
    return render(request, "frontend/admin/report_users.html", context)


@login_required
def projects_report_view(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    total_projects = []

    projects = Project.objects.filter(account_checking__isnull=False)
    for i, project in enumerate(projects):
        print(f"Calculando balances de proyecto {project.code} ({i}/{len(projects)})")
        data_project = {"code": project.code, "name": project.name}
        data_project["account_checking"] = odoo.accounts.get_total_balances(
            project.account_checking, "bank"
        )
        data_project["account_expenses"] = odoo.accounts.get_total_balances(
            project.account_expenses
        )
        data_project["account_sales"] = odoo.accounts.get_total_balances(
            project.account_sales
        )
        total_invoices_untaxed, total_unpaid_expenses = (
            odoo.accounts.get_project_balances(project.code, f"G.{project.code}")
        )
        data_project["total_invoices_untaxed"] = total_invoices_untaxed
        data_project["total_unpaid_expenses"] = total_unpaid_expenses
        if None not in [
            data_project["account_expenses"],
            data_project["account_sales"],
        ]:
            data_project["total_unjustified"] = (
                data_project["account_expenses"]
                + data_project["account_sales"]
                - data_project["total_invoices_untaxed"]
                - data_project["total_unpaid_expenses"]
            )
        else:
            data_project["total_unjustified"] = None

        total_projects.append(data_project)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "reports",
        "user_id": request.user.id,
        "total_projects": total_projects,
    }
    context = add_menu_context(request, context, "reports")
    return render(request, "frontend/admin/report_projects.html", context)


@csrf_exempt
@login_required
def new_cashin_view(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    if request.method == "POST":
        # Procesa los datos POST si la solicitud es de tipo POST
        amount = request.POST.get("received_amount")
        unpaid_invoice = request.POST.get("unpaid_invoice")
        notify_project = (
            True
            if "notify_project" in request.POST
            and request.POST.get("notify_project") == "on"
            else False
        )
        pay_taxes = (
            True
            if "pay_taxes" in request.POST and request.POST.get("pay_taxes") == "on"
            else False
        )

        cashin_date = request.POST.get("cashin_date")
        amount = float(amount)
        from_journal = request.POST.get("from_journal")
        reference = request.POST.get("reference")
        if not unpaid_invoice:
            # Cash in without invoice attached
            account_credit = settings.ODOO_ACCOUNT_CLIENTS
            account_debit = odoo.accounts.get_code_from_account_id(
                odoo.accounts.get_account_id_from_journal(from_journal)
            )

            # In case there is no invoice attached, we create the account move based on the form details manually
            object_id, message = odoo.accounts.create_bank_account_move(
                account_debit, account_credit, amount, reference, move_date=cashin_date
            )
            if object_id:
                messages.success(request, f"Éxito en la operación.")
            else:
                messages.error(request, f"Error en la operación: {message}.")
        else:
            invoice = odoo.invoices.get_invoice_by_name(unpaid_invoice)
            if not invoice:
                messages.error(
                    request, f"Error encontrando la factura {unpaid_invoice}."
                )
            else:
                project_code = invoice["name"].split("/")[1]
                ok = odoo.accounts.create_cashin_account_move(
                    invoice,
                    journal_id=from_journal,
                    move_date=cashin_date,
                    project_code=project_code,
                    amount=amount,
                    pay_taxes=pay_taxes,
                )
                if not ok:
                    messages.error(
                        request,
                        f"Error pagando la factura {unpaid_invoice}. Mira los logs.",
                    )
                else:
                    messages.success(
                        request, f"Éxito pagando la factura {unpaid_invoice}."
                    )
                    details = {
                        "action_username": request.user.username,
                        "invoice_name": invoice["name"],
                        "amount_untaxed": invoice["amount_untaxed"],
                        "amount_total": amount,
                        "project_code": project_code,
                        "pay_taxes": pay_taxes,
                    }
                    # odoo.utils.send_email_new_account_move(details)
                    if notify_project:
                        odoo.utils.send_email_new_cashin(details)
                        messages.success(
                            request,
                            f"Éxito notificando por mail a los admins del proyecto.",
                        )

    unpaid_invoices = odoo.invoices.get_all_unpaid_invoices()
    all_projects = list(Project.objects.values("code", "name"))
    payment_accounts = odoo.accounts.get_payment_journals()
    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "unpaid_invoices": unpaid_invoices,
        "all_projects": all_projects,
        "user_id": request.user.id,
        "payment_accounts": payment_accounts,
        "navbar": "new_cashin",
    }

    context = add_menu_context(request, context, "new_cashin")
    return render(request, "frontend/admin/new_cashin.html", context)


@csrf_exempt
@login_required
def new_project_view(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": request.user.id,
        "navbar": "new_project",
    }
    if request.method == "POST":
        # Procesa los datos POST si la solicitud es de tipo POST
        project_code = request.POST.get("project_code")
        project_code = project_code[0:3]  # Get only the first 3 characters
        project_name = request.POST.get("project_name")
        project_is_member = (
            True
            if "project_is_member" in request.POST
            and request.POST.get("project_is_member") == "on"
            else False
        )
        project, created = Project.objects.get_or_create(
            code=project_code, defaults={"name": project_name}
        )
        details = odoo.accounts.create_project(
            project, project_is_member=project_is_member
        )
        details["created"] = created
        details["name"] = project_name
        details["code"] = project_code
        print("Project created or updated:")

        email_details = {
            "action_username": request.user.username,
            "project": project,
            "update_details": details,
        }
        odoo.utils.send_email_new_group_created(email_details)
        context["update_details"] = details
        context["project"] = project

    context = add_menu_context(request, context, "new_project")
    return render(request, "frontend/admin/new_project.html", context)


@csrf_exempt
@login_required
def new_user_view(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "user_id": request.user.id,
        "navbar": "new_user",
    }

    if request.method == "POST":
        user_email = request.POST.get("user_email")
        user_firstname = request.POST.get("user_firstname")
        user_lastname = request.POST.get("user_lastname")
        user_is_member = (
            True
            if "user_is_member" in request.POST
            and request.POST.get("user_is_member") == "on"
            else False
        )

        user, created = User.objects.get_or_create(
            username=user_email,
            defaults={
                "email": user_email,
                "first_name": user_firstname,
                "last_name": user_lastname,
            },
        )
        if created:
            password = utils.generate_password()
            print(f"Registrando usuario GNB para {user_email} con password: {password}")
            context["password"] = password
            user.set_password(password)
        user.is_active = True
        user.save()

        details = odoo.accounts.create_user(user, user_is_member=user_is_member)

        details["created"] = created
        details["email"] = user.email
        details["first_name"] = user.first_name
        details["last_name"] = user.last_name

        context["update_details"] = details
        context["object"] = user
        print("User created or updated:")
        pprint(context)

        email_details = {
            "action_username": request.user.username,
            "user": user,
            "update_details": details,
        }
        odoo.utils.send_email_new_user_created(email_details)

    context = add_menu_context(request, context, "new_user")
    return render(request, "frontend/admin/new_user.html", context)


def get_all_projects_balances(user):
    total_projects = {}
    if user:
        projects = user.project_set.all()
    else:
        projects = Project.objects.all()

    for project in projects:
        cash_accounts = get_cash_accounts(project)
        project_total, _ = get_project_total(
            project, cash_accounts, show_donations=False
        )
        if user:
            m = ProjectMembership.objects.filter(user=user, project=project).first()
            m = project_total["membership_role"] = m.role
        total_projects[project.code] = project_total

    return total_projects


def get_project_total(project, cash_accounts={}, show_donations=True):
    transactions = []
    print("Getting balances for project", project.code)
    project_total = {}
    project_total["account_checking"] = odoo.accounts.get_total_balances(
        project.account_checking, "bank"
    )
    project_total["account_expenses"] = odoo.accounts.get_total_balances(
        project.account_expenses
    )
    project_total["account_sales"] = odoo.accounts.get_total_balances(
        project.account_sales
    )
    project_total["account_loans"] = odoo.accounts.get_total_balances(
        project.account_loans
    )
    print("Getting balance unpaid expenses and invoices...")
    total_invoices_untaxed, total_unpaid_expenses = odoo.accounts.get_project_balances(
        project.code, f"G.{project.code}"
    )
    project_total["total_invoices_untaxed"] = total_invoices_untaxed
    project_total["total_unpaid_expenses"] = total_unpaid_expenses
    project_total["name"] = project.name

    if None not in [
        project_total["account_checking"],
        project_total["account_expenses"],
        project_total["account_sales"],
    ]:
        project_total["unjustified_total"] = (
            project_total["account_expenses"]
            + project_total["account_sales"]
            - project_total["total_invoices_untaxed"]
            - project_total["total_unpaid_expenses"]
        )
        transactions = {
            "checking": odoo.accounts.get_transactions(project.account_checking)
        }
        if show_donations:
            project_total["why_donations_url"] = settings.WHY_DONATIONS_URL

            if project.percentage_recomended_income_donate:
                recommended_donations = (
                    project_total["account_sales"]
                    - project_total["total_invoices_untaxed"]
                ) * (project.percentage_recomended_income_donate / 100)
            else:
                recommended_donations = 0

            project_total["made_donations"] = odoo.accounts.get_made_donations(
                project.code, transactions["checking"]
            )
            difference = recommended_donations - project_total["made_donations"]
            if difference >= 0:
                project_total["recommended_extra_donations"] = difference
            else:
                project_total["recommended_extra_donations"] = 0

            project_total["why_donations_url"] = settings.WHY_DONATIONS_URL

            if recommended_donations:
                if project_total["made_donations"] > 0:
                    project_total["percentage_donations"] = (
                        project_total["made_donations"] / recommended_donations
                    ) * 100
                else:
                    project_total["percentage_donations"] = 0
            else:
                project_total["percentage_donations"] = None
        else:
            project_total["recommended_extra_donations"] = "Ver detalles"
            project_total["made_donations"] = "Ver detalles"

        if project_total["unjustified_total"] > 0:
            project_total["available_balance"] = (
                project_total["account_checking"] - project_total["unjustified_total"]
            )
        else:
            project_total["available_balance"] = project_total["account_checking"]

        for account_code, v in cash_accounts.items():
            project_total["available_balance"] += v["balance"]
            transactions[v["name"]] = odoo.accounts.get_transactions(account_code)

    else:
        project_total["unjustified_total"] = None
        project_total["available_balance"] = None
        project_total["recommended_extra_donations"] = None
        project_total["made_donations"] = None

    return project_total, transactions


@check_user_access
@login_required
def project_view(request, project_code, user):
    project = Project.objects.filter(code=project_code).first()
    if not project:
        return HttpResponseNotFound("Proyecto no encontrado.")

    is_member = ProjectMembership.objects.filter(user=user, project=project).exists()

    if not is_member and not request.user.is_superuser:
        return HttpResponseForbidden("Access denied. Event reported to admins.")

    invoices = []
    transactions = []

    cash_accounts = get_cash_accounts(project)
    project_members = get_project_members(project)
    if project.is_main_project:  # IF IS THE MAIN PROJECT OF GNB
        cash_account_codes = cash_accounts.keys()
        accounts = odoo.accounts.get_liquidity_accounts()
        balances = {}
        total_balance = main_project_available = 0
        balance_account_checking = odoo.accounts.get_total_balances(
            project.account_checking, "bank"
        )
        print("Suming balances for Bank Accounts")
        for account in accounts:
            code = account["code"]
            balance = odoo.accounts.get_total_balances(code, "bank")
            if code not in cash_account_codes:
                total_balance += balance
                main_project_available += balance
                balances[code] = balance
        print("Suming balances for Cash Accounts")
        for code in cash_account_codes:
            balance = odoo.accounts.get_total_balances(code, "bank")
            main_project_available += balance

        cash_accounts[0] = {"name": "Cuenta principal", "balance": total_balance}
        project_total = {
            "available_balance": main_project_available,
            "account_checking": balance_account_checking,
        }

        context = {
            "meta_opengraph": "Metadata info",
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
            "navbar": "project_view",
            "user_id": user.id,
            "project_total": project_total,
            "project": project,
            "members": project_members,
            "cash_accounts": cash_accounts,
            "transactions": transactions,
            "invoices": invoices,
        }

    else:  # FOR ANY OTHER PROJECT
        journal_id = odoo.accounts.get_journal_id_from_code(f"G.{project.code}")
        print("journal_id: {0}".format(journal_id))
        expenses = odoo.expenses.get_expenses_from_journal(journal_id)
        project_total, transactions = get_project_total(
            project, cash_accounts, show_donations=True
        )
        invoices = odoo.invoices.get_invoices_by_project(project.code)

        print("expenses", expenses)
        user_is_project_admin = (
            user.email in project_members and project_members[user.email] == "admin"
        )
        user_data = User.objects.all().values("id", "email", "first_name", "last_name")
        user_dict = {
            item["email"]: item["first_name"] + item["last_name"] for item in user_data
        }
        project_data = Project.objects.all().values("code", "name")
        project_dict = {item["code"]: item["name"] for item in project_data}
        # Combine the dictionaries
        all_accounts = {**user_dict, **project_dict}
        context = {
            "meta_opengraph": "Metadata info",
            "unread_count": request.user.notifications.unread().count(),
            "notifications": request.user.notifications.all(),
            "navbar": "project_view",
            "user_is_project_admin": user_is_project_admin,
            "user_id": user.id,
            "user_email": user.email,
            "project_total": project_total,
            "project": project,
            "members": project_members,
            "available_balance": project_total["available_balance"],
            "from_account": project.code,
            "cash_accounts": cash_accounts,
            "transactions": transactions,
            "all_accounts": all_accounts,
            "expenses": expenses,
            "invoices": invoices,
        }
    context = add_menu_context(request, context, "project_view")
    return render(request, "frontend/get_project.html", context)


def get_cash_accounts(project):
    cash_accounts = {}
    for c in project.cash_accounts.all():
        account_code = str(c.account_number)
        total = odoo.accounts.get_total_balances(account_code, "bank")
        cash_accounts[account_code] = {"name": c.name}
        cash_accounts[account_code]["balance"] = total
        cash_accounts[account_code]["admins"] = c.user_admins.all()
        cash_accounts[account_code]["id"] = c.id
    return cash_accounts


def get_project_members(project):
    members = {}
    for m in project.memberships.all():
        members[m.user.email] = m.role
    return members


@check_user_access
@login_required
def my_dashboard_view(request, user):
    user_projects = utils.get_user_projects(user, exclude_main=False)
    user_data = User.objects.all().values("id", "email", "first_name", "last_name")
    user_dict = {
        item["email"]: item["first_name"] + item["last_name"] for item in user_data
    }
    project_data = Project.objects.all().values("code", "name")
    project_dict = {item["code"]: item["name"] for item in project_data}
    # Combine the dictionaries
    all_accounts = {**user_dict, **project_dict}

    total_personal = {}
    total_personal["name"] = user.first_name
    total_personal["account_checking"] = odoo.accounts.get_total_balances(
        user.odooprofile.account_checking, "bank"
    )
    total_personal["account_loans"] = odoo.accounts.get_total_balances(
        user.odooprofile.account_loans
    )
    account_checking = user.odooprofile.account_checking
    print("#Dashboard: Getting user txs")

    transactions = {"checking": odoo.accounts.get_transactions(account_checking)}

    print("#Dashboard: Getting user balances")
    reimbursable_social_capital = (
        0
        if not user.odooprofile.reimbursable_social_capital
        else user.odooprofile.reimbursable_social_capital
    )
    account_social_capital = odoo.accounts.get_total_balances(
        user.odooprofile.account_social_capital
    )
    account_social_capital = 0 if not account_social_capital else account_social_capital
    total_personal["account_social_capital"] = (
        account_social_capital + reimbursable_social_capital
    )
    print("#Dashboard: Getting user projects data")
    # total_projects = get_all_projects_balances(user)

    context = {
        "meta_opengraph": "Metadata info",
        "unread_count": request.user.notifications.unread().count(),
        "notifications": request.user.notifications.all(),
        "navbar": "my_dashboard",
        "user_id": user.id,
        "total_personal": total_personal,
        "projects": project_dict,
        "from_account": user.email,
        "available_balance": total_personal["account_checking"],
        "user_email": user.email,
        "user_projects": user_projects,
        "all_accounts": all_accounts,
        "transactions": transactions,
    }
    context = add_menu_context(request, context, "my_dashboard")
    return render(request, "frontend/my_dashboard.html", context)
