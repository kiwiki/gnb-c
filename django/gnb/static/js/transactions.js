function openConfirmModal() {
  formValid = validateForm();
  if (!formValid) {
    alert("Por favor, complete todos los campos requeridos.");
  }
  else {
    formValid = validatePaymentData();
    if (formValid) {
      var $to_account = $('input[name="destination_type"]:checked').val() === 'internal_account'
        ? $('#to_account_internal').find('option:selected').text()
        : $('#to_account_external').val();

      var bodyContent = '¿Está seguro de querer realizar la siguiente transferencia?<br><b>Origen</b>: ' + $('#from_account').val() + '<br><b>Destino</b>: ' + $to_account + '<br><b>Cantidad</b>: ' + $('#transfer_amount').val() + ' €<br><b>Referencia</b>: ' + $('#transfer_ref').val();
      var title = 'Confirmar Transferencia';
      var footerContent = `
          <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cancelar</button>
          <button type="button" class="btn btn-primary" onclick="sendPaymentForm()">Confirmar</button>
        `;
      createDynamicModal(title, bodyContent, footerContent);
    }
  }
}


function validatePaymentData() {
  var account_available = parseFloat($('#available_balance').val());
  var amount_input = $('#transfer_amount')
  var transfer_amount = parseFloat(amount_input.val());
  var valid = true;
  if (isNaN(account_available) || isNaN(transfer_amount) || transfer_amount <= 0) {
    alert("Por favor, ingrese una cantidad válida.");
    amount_input.addClass("is-invalid");
    valid = false;
  }
  if (transfer_amount > account_available) {
    alert("La cantidad de transferencia no debe exceder el saldo disponible.");
    amount_input.addClass("is-invalid");
    valid = false;
  }
  if ($('input[name="destination_type"]:checked').val() === 'internal_account') {
    $from_account_val = $('#from_account option:selected').val();
    $to_account_val = $('#to_account_internal option:selected').val();
    if ($from_account_val === $to_account_val) {
      $('#to_account_internal').next('.dropdown-toggle').addClass("is-invalid");
      $('#from_account').next('.dropdown-toggle').addClass("is-invalid");
      alert("Origen y destino no pueden ser el mismo");
      valid = false;
    }

  } else {
    $to_account_external = $('#to_account_external')
    if ($to_account_external.val() === '') {
      $to_account_external.addClass("is-invalid");
      alert("Debes añadir una cuenta SEPA destinataria para la transferencia.");
      valid = false;
    }
  }


  return valid;
}

function sendPaymentForm() {
  formValid = validateForm();
  if (!formValid) {
    alert("Por favor, complete todos los campos requeridos.");
  }
  else {
    formValid = validatePaymentData();
    if (formValid) {
      var formValues = {};
      var formData = new FormData($('#new_account_move')[0]); // Create a FormData object with the form data
      var user_id = $('#user_id').val();
      closeConfirmModal();
      showLoadingScreen();
      $.ajax({
        url: '/odoo/account_move/', // Specify the URL for your Django view
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
          // Handle the response from Django
          if (response.object_id === null) {
            // Si invoice_id es nulo, muestra el mensaje como un error
            alert('Error found: ' + response.message); // Puedes reemplazar esto con tu propia lógica para mostrar el mensaje de error.
          } else {
            var bodyContent = 'Transferencia exitosa';
            var title = 'Transferencia exitosa';
            var footerContent = `
                        <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cerrar</button>
                      `;
            hideLoadingScreen();
            createDynamicModal(title, bodyContent, footerContent);

            location.reload();
            // var url = '/gnb/dashboard/';
            // if (user_id) { url = url + "?user=" + user_id; }
            // window.location.href = url;
          }
        },
        error: function (xhr, status, error) {
          // Handle errors
          console.log('Error:', error);
          alert('Error: ' + xhr.responseJSON.error); // Muestra el mensaje de error enviado desde el backend 
          hideLoadingScreen();
        }
      });
    }
  }
}

document.addEventListener('DOMContentLoaded', function () {
  // Obtén todos los elementos con la clase 'accordion-button'
  var buttons = document.querySelectorAll('.accordion-button');

  // Itera sobre cada botón
  buttons.forEach(function (button) {
    // Obtén el span dentro del botón actual
    var span = button.querySelector('.badge');

    // Verifica si el span y el botón actual existen antes de continuar
    if (span && button) {
      var valueText = span.innerText.trim();

      // Verifica si el texto indica un valor negativo y cambia la clase de fondo en consecuencia
      if (valueText.startsWith('0.')) {
        button.classList.remove('alert-info');
        button.classList.add('alert-success');

        span.classList.remove('bg-info');
        span.classList.add('bg-success');
      }
      else if (valueText.startsWith('-')) {
        // Accede a la div anterior al span y cambia su texto
        var previousDiv = button.querySelector('.justify-content-between.align-items-center');
        if (previousDiv) {
          previousDiv.textContent = "Justificado extra";
        }
        span.innerText = valueText.replace('-', '');
      }
      else {
        button.classList.remove('alert-info');
        button.classList.add('alert-warning');
        span.classList.remove('bg-info');
        span.classList.add('bg-warning');
      }
    }
  });
  var transactionsTableRows = document.querySelectorAll('#table-transactions tbody tr');

  transactionsTableRows.forEach(function (row) {
    var deuda = parseFloat(row.querySelector('td:nth-child(4)').textContent.trim().replace('€', ''));
    var estado = row.querySelector('td:nth-child(6)').textContent.trim(); // Cambiado de 5 a 6
    var estadoCell = row.querySelector('td:nth-child(6)'); // Cambiado de 5 a 6

    if (estado === 'posted') {
      row.classList.add('table-success');
      estadoCell.textContent = 'Enviada';
    } else if (estado === 'cancel') {
      row.classList.add('table-danger');
      estadoCell.textContent = 'Cancelada';
    }

    if (deuda < 0) {
      row.classList.add('table-warning');
    }
  });
  var invoicesTableRows = document.querySelectorAll('#table-invoices tbody tr');
  invoicesTableRows.forEach(function (row) {
    var deuda = parseFloat(row.querySelector('td:nth-child(4)').textContent.trim().replace('€', ''));
    var estado = row.querySelector('td:nth-child(6)').textContent.trim(); // Cambiado de 5 a 6
    var estadoCell = row.querySelector('td:nth-child(6)'); // Cambiado de 5 a 6

    if (estado === 'posted') {
      row.classList.add('table-success');
      estadoCell.textContent = 'Enviada';
    } else if (estado === 'cancel') {
      row.classList.add('table-danger');
      estadoCell.textContent = 'Cancelada';
    }

    if (deuda < 0) {
      row.classList.add('table-warning');
    }
  });
});

$(document).ready(function () {
  // Al cargar la página, asegúrate de que el estado inicial esté correcto
  toggleDestinationAccountState();

  // Manejar cambios en los radio buttons
  $('input[name="destination_type"]').change(function () {
    toggleDestinationAccountState();
  });

  // Función para actualizar el estado de los elementos según el radio button seleccionado
  function toggleDestinationAccountState() {
    if ($('#internal_account').is(':checked')) {
      $('#to_account_internal').prop('disabled', false).selectpicker('refresh');
      $('#to_account_external').prop('disabled', true).val('');
    } else if ($('#external_account').is(':checked')) {
      $('#to_account_internal').prop('disabled', true).selectpicker('refresh');
      $('#to_account_external').prop('disabled', false);
    }
  }
});


function validateAndSendPayment() {
  var currentUrl = window.location.href;
  sendGeneralForm('/odoo/cash_account_move/', currentUrl)
}

function addCashAccountTx(id, name) {
  var bodyContent = `<form id="form">
      <input type="hidden" name="cash_account_id" value="`+ id + `">
      <label for="tx_direction" class="form-label">Tipo de movimiento:</label>
        <select name="tx_direction"><option value="in">Entrada</option><option value="out">Salida</option></select>
        <br><label for="address" class="form-label">Referencia:</label>
        <input type="text" class="form-control" name="tx_reference" value=""
          required>
        <br><label for="tx_amount" class="form-label">Cantidad</label>
        <input type="number" class="form-control" name="tx_amount"  style="width: 200px;"
        required min="0" step="0.01" pattern="\d+(\.\d{1,2})?" >
        <div class="invalid-feedback">
          Cantidad requerida
        </div>
                        </form>`;
  var title = 'Transferencia en ' + name;
  var footerContent = `
        <button type="button" class="btn btn-secondary" onclick="closeConfirmModal()">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="validateAndSendPayment()">Confirmar</button>
      `;
  createDynamicModal(title, bodyContent, footerContent);
}