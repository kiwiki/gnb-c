
// // Get the button:
// let mybutton = document.getElementById("btn-back-to-top");

// // When the user scrolls down 20px from the top of the document, show the button
// window.onscroll = function() {scrollFunction()};

// function scrollFunction() {
//   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
//     mybutton.style.display = "block";
//   } else {
//     mybutton.style.display = "none";
//   }
// }

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}


// Obtener la fecha actual en el formato deseado (YYYY-MM-DD)
function obtenerFechaActual() {
  const fecha = new Date();
  const year = fecha.getFullYear();
  const month = (fecha.getMonth() + 1).toString().padStart(2, '0'); // +1 porque enero es 0
  const day = fecha.getDate().toString().padStart(2, '0');
  return `${year}-${month}-${day}`;
}

function showLoadingScreen() {
  var loadingScreen = document.getElementById('loading-screen');
  loadingScreen.style.display = 'block';
  var loadingOverlay = document.getElementById('loading-overlay');
  loadingOverlay.style.display = 'block';
  document.body.style.overflow = 'hidden';
}

function hideLoadingScreen() {
  var loadingScreen = document.getElementById('loading-screen');
  loadingScreen.style.display = 'none';
  var loadingOverlay = document.getElementById('loading-overlay');
  loadingOverlay.style.display = 'none';
  document.body.style.overflow = 'auto';

}

function validateForm() {
  var requiredFields = document.querySelectorAll("[required]");
  var formValid = true;

  for (var i = 0; i < requiredFields.length; i++) {
    if (!requiredFields[i].value) {
      formValid = false;

      requiredFields[i].classList.add("is-invalid");
    }
  }
  return formValid;
}



function sendGeneralForm(action_address, success_address = '') {
  formValid = validateForm();
  if (!formValid) {
    alert("Por favor, complete todos los campos requeridos.");
  }
  else {
    var formValues = {};
    var formData = new FormData($('#form')[0]); // Create a FormData object with the form data
    //var user_id = $('#user_id').val();
    showLoadingScreen();
    $.ajax({
      url: action_address, // Specify the URL for your Django view
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success: function (response) {
        // Handle the response from Django
        if (response.object_id === null) {
          // Si invoice_id es nulo, muestra el mensaje como un error
          alert('Error found: ' + response.message); // Puedes reemplazar esto con tu propia lógica para mostrar el mensaje de error.
        } else {
          hideLoadingScreen();
          alert('Operación realizada con éxito');
          $('#dynamicModal').modal('hide');
          // // Si invoice_id no es nulo, redirige el navegador a otra página
          if (success_address.length) {
            var url = success_address.replace('<id>', response.object_id);; // Reemplaza 'URL_de_Redirección' con la URL a la que deseas redirigir.
            window.location.href = url;
          } else {
            location.reload();
          }
        }
      },
      error: function (xhr, status, error) {
        // Handle errors
        console.log('Error:', error);
        alert('Error: ' + xhr.responseJSON.error); // Muestra el mensaje de error enviado desde el backend
        hideLoadingScreen();
      }
    });
  }

}

function createDynamicModal(title, bodyContent, footerContent) {
  // Remove any existing modals
  $('#dynamicModal').remove();

  // Create the modal HTML
  var modalHtml = `
    <div class="modal fade" id="dynamicModal" tabindex="-1" role="dialog" aria-labelledby="dynamicModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="dynamicModalLabel">${title}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ${bodyContent}
          </div>
          <div class="modal-footer">
            ${footerContent}
          </div>
        </div>
      </div>
    </div>
  `;

  // Append the modal HTML to the body
  $('body').append(modalHtml);

  $('#dynamicModal .close').click(function () {
    $('#dynamicModal').modal('hide');
  });

  // Show the modal
  $('#dynamicModal').modal('show');
}

function closeConfirmModal() {
  $('#dynamicModal').modal('hide');
}
