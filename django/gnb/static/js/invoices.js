//VARIABLES GENERALES
//FUNCIONES


document.getElementById("taxIdInput").addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        SearchTaxID();
    }
});

// FUNCION DE LOCALIZAR CLIENTE
function SearchTaxID() {
    var taxIdinput = document.getElementById("taxIdInput")
    var taxIdValue = taxIdinput.value;
    var nameinput = document.getElementById("client_name");
    var streetinput = document.getElementById("client_street");
    var cityinput = document.getElementById("client_city");
    var zipinput = document.getElementById("client_zip");
    const url = "/odoo/tax_id/" + taxIdValue;
    fetch(url, {
        method: "GET",
    })
        .then(response => response.json())
        .then(data => {
            if (data.hasOwnProperty("error")) {
                if (data.error == "NOT_VALID_VAT_ID") alert("CIF inválido.");
                else alert("Error desconocido buscando el CIF.");
            }
            else {
                if (data.id === null) {
                    alert("Cliente no encontrado, por favor rellena los datos del cliente abajo.");
                    zipinput.removeAttribute("disabled");
                    nameinput.removeAttribute("disabled");
                    streetinput.removeAttribute("disabled");
                    cityinput.removeAttribute("disabled");
                } else {
                    nameinput.value = data.name;
                    taxIdinput.value = data.vat;
                    streetinput.value = data.street;
                    cityinput.value = data.city;
                    zipinput.value = data.zip;
                    zipinput.setAttribute("disabled", "disabled");
                    nameinput.setAttribute("disabled", "disabled");
                    streetinput.setAttribute("disabled", "disabled");
                    cityinput.setAttribute("disabled", "disabled");
                }
            }
        })
        .catch(error => {
            console.error("Error en la solicitud AJAX:", error);
        });
}

// FUNCION RESTRINGIR
// Función para actualizar y seleccionar las opciones en todos los selects que contienen "product-"
function updateProductSelects() {
    const journalSelect = document.getElementById("journal");
    const selectedLetters = journalSelect.value.substring(0, 3).toUpperCase();

    // Obtener todos los selects que tienen un id que contiene "product-"
    const productSelects = document.querySelectorAll('[id*="product-"]');

    // Iterar a través de los selects y actualizar sus opciones
    productSelects.forEach(function (productSelect) {
        const options = productSelect.getElementsByTagName("option");
        for (let i = 0; i < options.length; i++) {
            const option = options[i];
            const optionRef = option.getAttribute("ref").toUpperCase();

            // Comprobar si el campo ref de la opción contiene las tres primeras letras
            if (optionRef.includes(selectedLetters)) {
                // Mostrar y seleccionar la opción
                option.style.display = "block";
                option.selected = true;
            } else {
                // Ocultar las opciones que no coinciden
                option.style.display = "none";
            }
        }
    });
}

// Asignar el evento onchange al select journal para llamar a la función de actualización
const journalSelect = document.getElementById("journal");
journalSelect.addEventListener("change", updateProductSelects);

// Llamar a la función de actualización al cargar la página para establecer el estado inicial
updateProductSelects();

// FUNCION AÑADIR LÍNEAS
var rowCount = 0; // Inicializamos el contador en 1

// Agregar un evento de clic al botón 'add-row-button'
$(document).on('click', '.add-row-button', function () {
    // Clonar la fila más cercana

    var newRow = $('#new_invoice .input-row').last().clone();

    // Incrementar el contador y utilizarlo para crear IDs únicos
    rowCount++;
    var newRowId = 'inputRow-' + rowCount;
    var newProductSelectId = 'product-' + rowCount;
    var newDescriptionInputId = 'description-' + rowCount;
    var newTaxesSelectId = 'taxes-' + rowCount;
    var newQuantityInputId = 'quantity-' + rowCount;
    var newPriceInputId = 'price-' + rowCount;

    newRow.attr('id', newRowId);
    productElement = newRow.find('[id^="product-"]')
    productElement.attr('id', newProductSelectId);
    //selectProductFromJournal(productElement, $('#journal').val());
    newRow.find('[id^="description-"]').attr('id', newDescriptionInputId);
    newRow.find('[id^="taxes-"]').attr('id', newTaxesSelectId);
    newRow.find('[id^="quantity-"]').attr('id', newQuantityInputId);
    newRow.find('[id^="price-"]').attr('id', newPriceInputId);

    // Limpiar los valores de los campos de la nueva fila
    newRow.find('input[type="text"]').val('');
    newRow.find('input[type="number"]').val('');
    newRow.find('[id^="taxes-"]').val(2);
    newRow.find('[id^="quantity-"]').val("1");
    newRow.find('[id^="price-"]').val("1.00");

    // Insertar la nueva fila después de la fila actual
    $('#new_invoice .input-row').last().after(newRow);
    updateProductSelects();
    recalculateTotals();
});

function recalculateTotals() {
    // Cambiar el contenido de los elementos span por su id
    var totalWithoutTaxes = 0;
    var totalTaxes = 0;
    var total = 0;
    $('#new_invoice .input-row').each(function () {
        var newRow = $(this);

        // Assuming there are elements with IDs starting with 'taxes-', 'quantity-', and 'price-' inside each '.input-row'
        var taxesValue = newRow.find('[id^="taxes-"]').find(':selected').data('tax-amount');
        var quantityValue = newRow.find('[id^="quantity-"]').val();
        var priceValue = newRow.find('[id^="price-"]').val();


        if (!isNaN((taxesValue)) && !isNaN((quantityValue)) && !isNaN((priceValue))) {
            var rowWithoutTaxes = (quantityValue * priceValue);
            var rowTaxes = (rowWithoutTaxes * (taxesValue / 100)) // Assuming taxesValue is in percentage
            totalWithoutTaxes += rowWithoutTaxes;
            totalTaxes += rowTaxes;
            total = totalWithoutTaxes + totalTaxes
        }
    });

    $("#subtotalValue").text(totalWithoutTaxes.toFixed(2));
    $("#taxesValue").text(totalTaxes.toFixed(2));
    $("#totalValue").text(total.toFixed(2));
};

$(document).on('click', '.rem-row-button', function () {
    // Obtener todas las filas
    var rows = $('.input-row');

    // Verificar si hay más de una fila antes de eliminar
    if (rows.length > 1) {
        // Eliminar la fila actual
        $(this).closest('.input-row').remove();
    } else {
        // Si solo hay una fila, borra solo los valores en lugar de eliminarla
        $(this).closest('.input-row').find('input[type="text"]').val('');
        $(this).closest('.input-row').find('select').val('');
        $(this).closest('.input-row').find('input[type="number"]').val('');
    }
    recalculateTotals();
});

$(document).on('change', 'input[id^="quantity-"]', function () {
    recalculateTotals();
});
$(document).on('change', 'input[id^="price-"]', function () {
    recalculateTotals();
});

$(document).on('change', 'select[id^="taxes-"]', function () {
    var selectedValue = $(this).val();
    var notaField = $('#note');

    if (selectedValue == '47') {
        // Si el valor seleccionado es '2' (IVA 21%), establecer la nota deseada
        notaField.val('Enseñanza exenta de IVA Artículo 20 Uno 10o de la Ley 37/1992 de 28 de diciembre del Impuesto sobre el Valor Añadido');
    } else {
        // Verificar si al menos una fila tiene IVA 21%
        var iva0Found = false;
        $('select[id^="taxes-"]').each(function () {
            if ($(this).val() == '47') {
                iva0Found = true;
                return false; // Salir del bucle al encontrar el primer IVA 21%
            }
        });

        if (!iva0Found) {
            // Si no se encuentra IVA 21% en ninguna fila, borrar la nota
            notaField.val('');
        }
    }
    recalculateTotals();
});

// FUNCION DE ENVIAR EL FORMULARIO
function sendForm() {
    // e.preventDefault(); // Prevent the form from actually submitting

    // Get all form elements
    var formElements = $('#new_invoice').elements;

    var formValues = {};
    var formElements = $('#new_invoice').find(':input, textarea, select');

    // Now, formElements contains all the form elements
    // You can iterate through them like this:
    formElements.each(function (index, element) {
        // Access individual form elements using $(element)
        var fieldName = $(element).attr('id');
        var fieldValue = $(element).val();
        formValues[fieldName] = fieldValue;
    });

    formValues["journal"] = $("#journal").val();
    // Imprimir los datos del formulario en la consola
    console.log(formValues);

    // Display the form data in an alert
    // alert(JSON.stringify(formValues, null, 2));
    const url = "/odoo/invoice/"
    const requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json", // Tipo de contenido JSON
        },
        body: JSON.stringify(formValues, null, 2), // Convierte el diccionario a JSON y lo envía como cuerpo de la solicitud
    };
    showLoadingScreen();
    // Realizar la solicitud POST
    fetch(url, requestOptions)
        .then((response) => response.json())
        .then((data) => {
            // Manejar la respuesta JSON recibida

            console.log(data); // Aquí puedes acceder a los datos devueltos en formato JSON
            if (data.invoice_id === null) {
                // Si invoice_id es nulo, muestra el mensaje como un error
                alert('Error found: ' + data.message); // Puedes reemplazar esto con tu propia lógica para mostrar el mensaje de error.
            } else {
                // Si invoice_id no es nulo, redirige el navegador a otra página
                var redirect_url = '/gnb/invoices/'; // Reemplaza 'URL_de_Redirección' con la URL a la que deseas redirigir.
                if (formValues["user_id"]) { redirect_url = redirect_url + "?user=" + formValues["user_id"]; }
                window.location.href = redirect_url;
            }
            hideLoadingScreen();
        })
        .catch((error) => {
            console.error("Error:", error);
            alert('Error: ' + xhr.responseJSON.error); // Muestra el mensaje de error enviado desde el backend
            hideLoadingScreen();
        });
}

//ACCIONES
// Agregar un evento de cambio al elemento select con el ID 'journal'
$('#journal').on('change', handleChangeJournal);
handleChangeJournal();
recalculateTotals();