
//VARIABLES GENERALES

//FUNCIONES
// Establecer la fecha actual como valor por defecto
document.getElementById('date').value = obtenerFechaActual();

// Agregar un evento de cambio al elemento select con el ID 'journal'
// function handleChangeJournal() {
//     // Obtener el valor seleccionado
//     var selectedValue = $('#journal').val().substring(0, 3);
//     // next_number = JournalNextNumbers[selectedValue]
//     // document.getElementById("nombre-factura").innerText = next_number;
// };

//FUNCIÓN RESTRINGIR PRODUCTO A PROYECTO
// Agregar un evento de cambio al elemento select con el ID 'journal'
$('#journal').on('change', function () {
    // Obtener el valor seleccionado en el elemento 'journal'
    var journalXXX = $('#journal').val();
    selectProductFromJournal($(this), journalXXX);
    // Iterar a través de los elementos select de productos
    $('select[id^="product-"]').each(function () {
        // Habilitar o deshabilitar opciones en función de si la referencia seleccionada está contenida en el atributo 'ref'

    });
});
// FUNCION AÑADIR LÍNEAS
var rowCount = 0; // Inicializamos el contador en 1


// Agregar un evento de clic al botón 'add-row-button'
$(document).on('click', '.add-row-button', function () {
    // Clonar la fila más cercana
    var newRow = $('#new_expense .input-row').last().clone();

    // Incrementar el contador y utilizarlo para crear IDs únicos
    rowCount++;
    newRow.find('[id*="-"]').each(function () {
        var oldID = $(this).attr('id');
        var matches = oldID.match(/-(\d+)$/);

        if (matches) {
            var number = parseInt(matches[1]);
            var newID = oldID.replace('-' + number, '-' + rowCount);
            $(this).attr('id', newID);
            $(this).attr('name', newID);

        }
    });

    // Inicializar los input fields
    newRow.find('input[type="text"]').val('');
    newRow.find('input[type="number"]').val('');
    newRow.find('[id^="taxes-"]').val(2); //21% VAT default
    newRow.find('[id^="quantity-"]').val("1");
    newRow.find('[id^="price-"]').val("1.00");
    newRow.find('[id="expense_number"]').html('Gasto ' + (rowCount + 1));


    // Insertar la nueva fila después de la fila actual
    $('#new_expense .input-row').last().after(newRow);
    resetTypeSelectTrigger();
    newRow.find("select[id^='typeselect-']").trigger("change");
    recalculateTotals();
});

function getInputRowIds() {
    var inputRowNumbers = [];
    $('[id^="inputRow-"]').each(function () {
        var match = this.id.match(/\d+/);
        if (match) {
            inputRowNumbers.push(match[0]);
        }
    });
    return inputRowNumbers;
}

$('input[name="type_expense"]').change(function () {
    ids = getInputRowIds();
    for (var i = 0; i < ids.length; i++) {
        toggleFields(ids[i]);
    }
    recalculateTotals();
});

function recalculateTotals() {
    // Cambiar el contenido de los elementos span por su id
    var totalWithoutTaxes = 0;
    var totalTaxes = 0;
    var total = 0;
    $('#new_expense .input-row').each(function () {
        var newRow = $(this);
        // Assuming there are elements with IDs starting with 'taxes-', 'quantity-', and 'price-' inside each '.input-row'
        var typeExpense = newRow.find('[name^="type_expense-"]:checked').val();
        // Log or do something with the checked radio value
        if (typeExpense == 'normal_expense') { var taxesValue = 0; }
        else { var taxesValue = newRow.find('[id^="taxes-"]').find(':selected').data('tax-amount'); }
        var quantityValue = newRow.find('[id^="quantity-"]').val();
        var priceValue = newRow.find('[id^="price-"]').val();
        if (!isNaN((taxesValue)) && !isNaN((quantityValue)) && !isNaN((priceValue))) {
            var rowWithoutTaxes = (quantityValue * priceValue);
            var rowTaxes = (rowWithoutTaxes * (taxesValue / 100)) // Assuming taxesValue is in percentage
            totalWithoutTaxes += rowWithoutTaxes;
            totalTaxes += rowTaxes;
            total = totalWithoutTaxes + totalTaxes
        }
    });

    $("#subtotalValue").text(totalWithoutTaxes.toFixed(2));
    $("#taxesValue").text(totalTaxes.toFixed(2));
    $("#totalValue").text(total.toFixed(2));
};

$(document).on('change', 'input[id^="quantity-"]', function () {
    recalculateTotals();
});
$(document).on('change', 'input[id^="price-"]', function () {
    recalculateTotals();
});
$(document).on('change', 'select[id^="taxes-"]', function () {
    recalculateTotals();
});

function resetTypeSelectTrigger() {
    $("select[id^='typeselect-']").change(function () {
        // Obtener el valor seleccionado
        var selectedValue = $(this).val();
        // Obtener el número del ID del select
        var idNumber = $(this).attr("id").replace("typeselect-", "");
        // Comprobar el valor seleccionado y escribir la frase correspondiente
        if (selectedValue === "tipo_normal") {
            $("#description-" + idNumber).val("");
        } else if (selectedValue === "tipo_viaje") {
            $("#description-" + idNumber).val("Escribe el origen y el destino.Ej. (Albacete - Murcia), añade como unidades el nº de km y como precio 0,26€/km");
        } else if (selectedValue === "tipo_dietas") {
            $("#description-" + idNumber).val("Escribe donde disfrutaste las dietas. Ej (Dieta en Albacete). El precio aprox. es 26€/día");
        } else if (selectedValue === "tipo_Pernoctas") {
            $("#description-" + idNumber).val("Escribe donde disfrutaste las pernoctas. Ej (Pernocta en Albacete). El precio aprox. es 53€/día");
        } else if (selectedValue === "tipo_producto") {
            $("#description-" + idNumber).val("Escribe la descripción concepto de gasto en texto.");
        }
    });
}

// Agregar un evento de clic al botón 'rem-row-button'
$(document).on('click', '.rem-row-button', function () {
    // Obtener todas las filas
    var rows = $('.input-row');

    // Verificar si hay más de una fila antes de eliminar
    if (rows.length > 1) {
        // Eliminar la fila actual
        $(this).closest('.input-row').remove();
    } else {
        // Si solo hay una fila, borra solo los valores en lugar de eliminarla
        $(this).closest('.input-row').find('input[type="text"]').val('');
        $(this).closest('.input-row').find('select').val('');
        $(this).closest('.input-row').find('input[type="number"]').val('');
    }
    recalculateTotals();
});

document.getElementById("iban_account").addEventListener("input", function () {
    var iban = this.value.replace(/\s/g, ""); // Elimina espacios en blanco
    if (iban.length >= 15) {
        var valid = isValidIBAN(iban); // Llama a la función de validación IBAN

        var ibanResult = document.getElementById("iban-result");
        ibanResult.innerHTML = valid ? "✓" : "❌";

        // Establece el color del icono según la validez
        if (valid) {
            ibanResult.style.color = "green";
        } else {
            ibanResult.style.color = "red";
        }
    }
});

// Función para validar el IBAN (puedes personalizarla según tus necesidades)
function isValidIBAN(iban) {
    return IBAN.isValid(iban); // true
}

//REVISAR CAMPOS HABILITADOS
function toggleFields(number) {
    var radio1Checked = document.getElementById('type_expense_normal').checked;
    var radio2Checked = document.getElementById('type_expense_receipt').checked;

    var referenciaDiv = document.getElementById('div-reference-' + number);
    var referenciaField = document.getElementById('reference-' + number);
    var fileInvoiceField = document.getElementById('div-file-' + number);
    var impuestosSel = document.getElementById('div-taxes-' + number);
    var impuestosField = document.getElementById('taxes-' + number);
    var selectField = document.getElementById('div-typeselect-' + number);
    var destinoDiv = document.getElementById('destino');
    var destinoField = document.getElementById('iban_account');
    var destinoField2 = document.getElementById('iban_receiver');
    var destinoField3 = document.getElementById('iban_concept');


    if (radio1Checked) {
        referenciaDiv.style.display = 'none';
        referenciaField.removeAttribute('required');
        fileInvoiceField.style.display = 'none';
        impuestosSel.style.display = 'none';
        impuestosField.removeAttribute('required');
        selectField.style.display = 'block';
        destinoDiv.classList.add('d-none'); // Agrega la clase 'd-none' a la div 'destino'
        destinoField.style.display = 'none';
        destinoField.removeAttribute('required');
        destinoField2.style.display = 'none';
        destinoField2.removeAttribute('required');
        destinoField3.style.display = 'none';
        destinoField3.removeAttribute('required');

    } else {
        referenciaDiv.style.display = 'block'; // O 'initial' si prefieres restaurar el estilo predeterminado
        referenciaField.setAttribute('required', 'required');
        fileInvoiceField.style.display = 'block';
        impuestosSel.style.display = 'block';
        impuestosField.setAttribute('required', 'required');
        selectField.style.display = 'none';
        destinoDiv.classList.remove('d-none'); // Remueve la clase 'd-none' de la div 'destino'
        destinoField.style.display = 'block';
        destinoField2.style.display = 'block';
        destinoField3.style.display = 'block';
    }
}

// Llama a la función para configurar los campos inicialmente
toggleFields(0);
$("#normal_expense-" + 0).change(function () {
    toggleFields(0);
});

$("#receipt_expense-" + 0).change(function () {
    toggleFields(0);
});
resetTypeSelectTrigger();

// FUNDION DE ENVIAR EL FORMULARIO
function sendForm() {
    var requiredFields = document.querySelectorAll("[required]");
    var formValid = true;
    for (var i = 0; i < requiredFields.length; i++) {
        if (!requiredFields[i].value) {
            formValid = false;

            requiredFields[i].classList.add("is-invalid");
        }
    }

    if (!formValid) {
        alert("Por favor, complete todos los campos requeridos.");
    }
    else {
        var formValues = {};
        var formData = new FormData($('#new_expense')[0]); // Create a FormData object with the form data
        var user_id = $('#user_id').val();
        showLoadingScreen();
        $.ajax({
            url: '/odoo/expense/', // Specify the URL for your Django view
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                // Handle the response from Django
                if (response.invoice_id === null) {
                    // Si invoice_id es nulo, muestra el mensaje como un error
                    alert('Error found: ' + response.message); // Puedes reemplazar esto con tu propia lógica para mostrar el mensaje de error.
                } else {
                    alert('Gasto creado con éxito');
                    // // Si invoice_id no es nulo, redirige el navegador a otra página
                    var url = '/gnb/expenses/'; // Reemplaza 'URL_de_Redirección' con la URL a la que deseas redirigir.
                    if (user_id) { url = url + "?user=" + user_id; }
                    window.location.href = url;
                }
                hideLoadingScreen();
            },
            error: function (xhr, status, error) {
                // Handle errors

                console.log('Error:', error);
                alert('Error: ' + xhr.responseJSON.error); // Muestra el mensaje de error enviado desde el backend 
                hideLoadingScreen();
            }
        });
    }
}

//ACCIONES
// Agregar un evento de cambio al elemento select con el ID 'journal'
$('#journal').on('change', handleChangeJournal);
handleChangeJournal();
recalculateTotals();