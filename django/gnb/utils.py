import datetime
import string
import secrets

CURRENT_YEAR = str(datetime.date.today().year)


def generate_password(length=12):
    characters = string.ascii_letters + string.digits + string.punctuation
    password = (
        secrets.choice(string.ascii_lowercase)
        + secrets.choice(string.ascii_uppercase)
        + secrets.choice(string.digits)
        + secrets.choice(string.punctuation)
    )
    password += "".join(secrets.choice(characters) for i in range(length - 4))
    password_list = list(password)
    secrets.SystemRandom().shuffle(password_list)
    return "".join(password_list)


def get_user_projects(user, exclude_main=False):
    queryset = user.project_memberships.select_related("project")
    if exclude_main:
        queryset = queryset.exclude(project__is_main_project=True)
    return {
        membership.project.code: {
            "name": membership.project.name,
            "role": membership.role,
            "archived": membership.project.archived,
            "can_invoice": membership.project.can_invoice,
        }
        for membership in queryset
    }
