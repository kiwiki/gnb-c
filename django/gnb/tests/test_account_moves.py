from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from unittest.mock import patch
from django.conf import settings
from gnb.tests import create_sample_user_with_odooprofile, create_sample_project


class BankAccountMoveViewTest(TestCase):
    def setUp(self):
        self.user = create_sample_user_with_odooprofile()
        self.sample_project, self.sample_membership = create_sample_project(self.user)
        self.client.login(username="testuser", password="password")
        self.client_noauth = Client()

    def test_invalid_account_move(self):
        # Simulate form input data
        data = {
            "from_account": "personal_account",
            "destination_type": "internal_account",
            "to_account_internal": self.sample_project.code,
            "transfer_amount": "0",
            "transfer_ref": "test",
            "avoid_notify": "off",
        }

        response = self.client_noauth.post("/odoo/account_move/", data=data)
        self.assertEqual(response.status_code, 302)
        # Check if the redirect URL starts with the expected login URL (ignoring query parameters)
        self.assertTrue(response.url.startswith(settings.LOGIN_URL))

        # Check error if amount is 0 or negative
        response = self.client.post("/odoo/account_move/", data=data)
        self.assertEqual(response.status_code, 400)
        json_response = response.json()
        ERROR_PARAMETERS_MSG = "Incorrect transaction parameters"
        self.assertTrue(json_response["error"].startswith(ERROR_PARAMETERS_MSG))

        data["transfer_amount"] = "-1"
        response = self.client.post("/odoo/account_move/", data=data)
        self.assertEqual(response.status_code, 400)
        json_response = response.json()
        ERROR_PARAMETERS_MSG = "Incorrect transaction parameters"
        self.assertTrue(json_response["error"].startswith(ERROR_PARAMETERS_MSG))

        # Missing from_account
        data = {
            "destination_type": "internal_account",
            "to_account_internal": self.sample_project.code,
            "transfer_amount": "10",
            "transfer_ref": "test",
        }
        response = self.client.post("/odoo/account_move/", data=data)
        self.assertEqual(response.status_code, 400)
        json_response = response.json()
        ERROR_PARAMETERS_MSG = "Incorrect transaction parameters"
        self.assertTrue(json_response["error"].startswith(ERROR_PARAMETERS_MSG))

    @patch("odoo.accounts")
    @patch("odoo.utils.send_mail")
    def test_valid_account_moves(self, mock_send_mail, mock_accounts):
        mock_accounts.create_bank_account_move.return_value = [100], None
        data = {
            "from_account": "personal_account",
            "destination_type": "internal_account",
            "to_account_internal": self.sample_project.code,
            "transfer_amount": "10",
            "transfer_ref": "test",
            "avoid_notify": "on",
        }

        response = self.client.post("/odoo/account_move/", data=data)
        self.assertEqual(response.status_code, 200)
        json_response = response.json()
        self.assertEqual(json_response, {"object_id": 100, "message": None})
        self.assertEqual(mock_send_mail.call_count, 1)

        call_args_list = mock_send_mail.call_args_list
        self.assertEqual(len(call_args_list), 1)  # Assuming only one email was sent
        call_args = call_args_list[0]
        email_subject = call_args[0][0]
        email_message = call_args[0][1]
        email_to = call_args[0][3]

        self.assertEqual(email_subject, "GNBAdmin: Nueva transacción.")
        self.assertIn(f"Cuenta origen: {self.user.email}", email_message)
        self.assertIn(f"Cuenta destino: {self.sample_project.code}", email_message)
        self.assertIn("Cantidad: 10.0 €", email_message)
        self.assertIn("Referencia: test", email_message)
        # Check that the email was sent to the administrator
        for admin_email in settings.ADMINS_EMAIL:
            self.assertIn(admin_email, email_to)

    @patch("odoo.accounts")
    @patch("odoo.utils.send_mail")
    def test_valid_account_moves_with_notification_to_none(
        self, mock_send_mail, mock_accounts
    ):
        mock_accounts.create_bank_account_move.return_value = [100], None
        data = {
            "from_account": "personal_account",
            "destination_type": "internal_account",
            "to_account_internal": self.sample_project.code,
            "transfer_amount": "10",
            "transfer_ref": "test",
            "avoid_notify": "off",
        }

        response = self.client.post("/odoo/account_move/", data=data)
        self.assertEqual(response.status_code, 200)
        json_response = response.json()
        self.assertEqual(json_response, {"object_id": 100, "message": None})
        self.assertEqual(mock_send_mail.call_count, 2)
        call_args_list = mock_send_mail.call_args_list
        self.assertEqual(len(call_args_list), 2)
        call_args = call_args_list[1]
        email_subject = call_args[0][0]
        email_message = call_args[0][1]
        email_to = call_args[0][3]

        self.assertEqual(email_subject, "GNB: Transacción recibida")
        self.assertIn(f"Cuenta origen: {self.user.email}", email_message)
        self.assertIn(f"Cuenta destino: {self.sample_project.code}", email_message)
        self.assertIn("Cantidad: 10.0 €", email_message)
        self.assertIn("Referencia: test", email_message)
        # Check that the email was sent to the administrator
        self.assertEqual(self.sample_membership.role, "member")
        self.assertEqual([], email_to)

    @patch("odoo.accounts")
    @patch("odoo.utils.send_mail")
    def test_valid_account_moves_with_notification_to_admin(
        self, mock_send_mail, mock_accounts
    ):
        mock_accounts.create_bank_account_move.return_value = [100], None
        data = {
            "from_account": "personal_account",
            "destination_type": "internal_account",
            "to_account_internal": self.sample_project.code,
            "transfer_amount": "10",
            "transfer_ref": "test",
            "avoid_notify": "off",
        }
        self.sample_membership.role = "admin"
        self.sample_membership.save()
        response = self.client.post("/odoo/account_move/", data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(mock_send_mail.call_count, 2)
        call_args_list = mock_send_mail.call_args_list
        call_args = call_args_list[1]

        email_to = call_args[0][3]
        self.assertEqual([self.user.email], email_to)
