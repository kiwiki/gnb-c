from django.test import TestCase, SimpleTestCase
from django.contrib.auth.models import User
from gnb.models import Project, ProjectMembership
from gnb.utils import *


class GeneratePasswordTest(SimpleTestCase):
    def test_password_length(self):
        password = generate_password(12)
        self.assertEqual(len(password), 12)

        password = generate_password(16)
        self.assertEqual(len(password), 16)

    def test_all_character_types_in_password(self):
        password = generate_password()
        self.assertTrue(any(char.islower() for char in password))
        self.assertTrue(any(char.isupper() for char in password))
        self.assertTrue(any(char.isdigit() for char in password))
        self.assertTrue(any(char in string.punctuation for char in password))


class GetProjectNamesTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="testuser", password="password")
        self.project1 = Project.objects.create(name="Project 1", code="P1")
        self.project2 = Project.objects.create(name="Project 2", code="P2")
        self.project3 = Project.objects.create(name="Project 3", code="P3")

    def test_get_user_projects_no_memberships(self):
        project_names = get_user_projects(self.user)
        self.assertEqual(project_names, [])

    def test_get_user_projects_single_membership(self):
        ProjectMembership.objects.create(
            user=self.user, project=self.project1, role="member"
        )
        project_names = get_user_projects(self.user)
        self.assertEqual(project_names, ["P1"])

    def test_get_user_projects_multiple_memberships(self):
        ProjectMembership.objects.create(
            user=self.user, project=self.project1, role="member"
        )
        ProjectMembership.objects.create(
            user=self.user, project=self.project2, role="admin"
        )
        ProjectMembership.objects.create(
            user=self.user, project=self.project3, role="member"
        )
        project_names = get_user_projects(self.user)
        self.assertEqual(len(project_names), 3)

    def test_get_user_projects_ignores_other_users(self):
        other_user = User.objects.create_user(username="otheruser", password="password")
        ProjectMembership.objects.create(
            user=self.user, project=self.project1, role="member"
        )
        ProjectMembership.objects.create(
            user=other_user, project=self.project2, role="admin"
        )
        project_names = get_user_projects(self.user)
        self.assertEqual(project_names, ["P1"])
