from django import template

register = template.Library()


@register.filter(name="change_sign")
def change_sign(value):
    if value:
        return value * -1
    else:
        return 0


@register.filter
def hash(h, key):
    return h.get(key, None)
