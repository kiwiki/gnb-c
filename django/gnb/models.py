# -*- coding: utf-8 -*-

from django.db import models
from django import forms
from django.utils import timezone
from django.utils.html import mark_safe
import datetime
from time import strftime
from django.contrib.sessions.models import Session
from django.core.validators import RegexValidator
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.template.defaultfilters import date as _date
from django.contrib.auth.models import User
from django.contrib import messages
import os
from django.utils import timezone
from django.template.defaultfilters import slugify
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.forms import widgets
import odoo.accounts


class PercentageField(models.FloatField):
    widget = widgets.TextInput(attrs={"class": "percentInput"})

    def to_python(self, value):
        val = super(PercentageField, self).to_python(value)
        if type(val) == int or float:
            return val / 100
        return val

    def prepare_value(self, value):
        val = super(PercentageField, self).prepare_value(value)
        if (type(val) == int or float) and not isinstance(val, str):
            return str((float(val) * 100))
        return val


class CharFieldWithTextarea(models.CharField):
    def formfield(self, **kwargs):
        kwargs.update({"widget": forms.Textarea(attrs={"rows": 3, "cols": 100})})
        return super(CharFieldWithTextarea, self).formfield(**kwargs)


class UserProfile(models.Model):
    user = models.OneToOneField(
        User,
        primary_key=True,
        on_delete=models.CASCADE,
    )
    language = models.CharField(
        _("Idioma interfaz"),
        max_length=15,
        choices=settings.LANGUAGE_CHOICES,
        default=settings.DEFAULT_LANGUAGE,
        help_text=_("El idioma de la interfaz."),
    )


class Project(models.Model):
    name = models.CharField(_("Nombre del proyecto"), max_length=100)
    code = models.CharField(_("Código del proyecto"), max_length=10, unique=True)
    is_main_project = models.BooleanField(_("Es el proyecto principal"), default=False)
    can_invoice = models.BooleanField(_("Tiene facturas"), default=True)
    archived = models.BooleanField(_("Archivado"), default=False)
    users = models.ManyToManyField(User, through="ProjectMembership")
    percentage_recomended_income_donate = models.FloatField(
        _("% de donaciones"), null=True, blank=True, default=10
    )
    account_checking = models.IntegerField(
        _("Cuenta corriente"), unique=True, null=True, blank=True
    )
    account_cash = models.IntegerField(
        _("Cuenta efectivo"), unique=True, null=True, blank=True
    )
    account_expenses = models.IntegerField(
        _("Cuenta gastos"), unique=True, null=True, blank=True
    )
    account_sales = models.IntegerField(
        _("Cuenta ingresos"), unique=True, null=True, blank=True
    )
    account_loans = models.IntegerField(
        _("Cuenta préstamos"), unique=True, null=True, blank=True
    )

    def __str__(self):
        return self.code


class CashAccount(models.Model):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="cash_accounts"
    )
    name = models.CharField(_("Nombre de la caja de efectivo"), max_length=100)
    user_admins = models.ManyToManyField(User, related_name="cash_accounts", blank=True)
    account_number = models.IntegerField(
        _("Número de cuenta"), null=True, blank=True, unique=True
    )

    def __str__(self):
        return f"Cash Account for Project {self.project.code}"


class ProjectMembership(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="project_memberships"
    )
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, blank=False, related_name="memberships"
    )
    ROLE_CHOICES = (
        ("member", "Miembro"),
        ("admin", "Administrador"),
        # Add more role options as needed
    )
    role = models.CharField(_("Rol"), max_length=10, choices=ROLE_CHOICES, blank=False)

    class Meta:
        unique_together = ("user", "project")  # Ensure uniqueness of user-project pairs
