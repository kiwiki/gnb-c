import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "Load the secret key in private"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

# Application definition
INSTALLED_APPS = [
    # "baton",  # always before django.contrib.admin
    "gnb",
    "odoo",  # In the future can be other ERPs or none.
    "django.contrib.admin",
    "django_registration",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "adminsortable2",
    "notifications",
    "debug_toolbar",  # only to use by devs, it is off by default
    #'django_extensions',#only to use by devs, ex. for generate png from graph_models
    "corsheaders",
    # "baton.autodiscover",  # very end
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = "gnbproject.urls"


from django.contrib.messages import constants as messages

MESSAGE_TAGS = {
    messages.DEBUG: "alert-secondary",
    messages.INFO: "alert-info",
    messages.SUCCESS: "alert-success",
    messages.WARNING: "alert-warning",
    messages.ERROR: "alert-danger",
}

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "gnb.context_processors.global_variables",
            ],
        },
    },
]

WSGI_APPLICATION = "gnbproject.wsgi.application"


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
# DATABASES = {
#     "default": {
#         "ENGINE": "django.db.backends.postgresql",
#         "NAME": "xxxx",
#         "USER": "xxxxx",
#         "PASSWORD": "123456789",
#         "HOST": "localhost",
#         "PORT": "5432",
#     }
# }
# LOAD YOUR DATABASES IN PRIVATE IN YOUR local_settings.py


# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        "OPTIONS": {
            "min_length": 7,
        },
    },
]
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

DEFAULT_LANGUAGE = "es"
LANGUAGE_CHOICES = (
    ("es-ES", "Castellano"),
    ("ca-ES", "Català"),
)
TIME_ZONE = "Europe/Madrid"
USE_I18N = True
USE_L10N = True
USE_TZ = True

REGISTRATION_OPEN = False
# change depending on domain
SIMPLE_BACKEND_REDIRECT_URL = "/"
LOGIN_URL = "/accounts/login"
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"
# Default
ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True
REGISTRATION_EMAIL_HTML = True

CACHE_TIME = 15 * 60  # Default cache time for some expensive operations

# to be used in pygraphviz + django-extensions
GRAPH_MODELS = {
    "all_applications": True,
    "group_models": True,
}
SITE_URL = "https://gnb.komun.org"
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

# Get from local.py in production
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "disroot.org"
EMAIL_HOST_USER = "xxxxxxxxx"
EMAIL_HOST_PASSWORD = "xxxxxxxxxxxxxx"
DEFAULT_FROM_EMAIL = "xxxxxxxx"
DEFAULT_REPLYTO_EMAIL = "xxxxxxxxxx"

EMAIL_PORT = 587
EMAIL_USE_TLS = True

STATIC_URL = "/static/"
print("BASE_DIR", BASE_DIR)
STATIC_ROOT = os.path.join(BASE_DIR, "static")
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"
# STATICFILES_DIRS = (os.path.join(BASE_DIR, "baton", "static"),)
# useful to apply python manage.py collectstatic and use different baton logo for example


try:
    from .local_settings import *
except:
    pass
