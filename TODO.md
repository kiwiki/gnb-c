# TODO

## Reescribir Descripción
Aplicación bancaria independiente, que permite la federación y la autogestión de colectivos y proyectos.
Ideas fuerza -> La banca ética no es suficiente, necesitamos crear y controlar nuestras economías.

## API
Grupos de control
Crear un frontend Django

### Funciones

#### Función sacar saldos: 

- DATOS PARA SACAR SALDO

##### Concepto
- Saldo de proyecto sin justificar: ((balance de reservas de proyecto) + (balance ingresos de proyecto) - (gastos de proyecto)) - ((facturas impagadas) + (gastos impagados)) DONE!.
- Saldo personal o del proyecto en cuenta corriente.

#### Función transferencia.

- Una transferencia es un movimiento entre cuentas, posibilidades:
- Cuenta corriente personal-a -> Cuenta corriente personal-b (Un pago entre personas)
  - Si hay comisión se genera automáticamente una transferencia entre la cuenta corriente personal-a --> Donaciones general de la entidad administradora.
  
- Cuenta corriente personal-a -> Cuenta corriente proyecto (Un pago a proyecto). Se podrá buscar por las siglas o el nombre del proyecto.
  - Si hay comisión se genera automáticamente una transferencia entre la cuenta corriente personal-a --> Donaciones general de la entidad administradora.

- Cuenta corriente personal -> Cuenta externa (Un pago externo)
  - Si hay comisión se genera automáticamente una transferencia entre la cuenta corriente personal-a --> Donaciones general de la entidad administradora.

- Transferencia desde una cuenta de proyecto. Esta transferencia incluye una generación de gasto justificando la salida de dinero del proyecto (garantizando así el buen uso contable de la entidad administradora) -> el dinero va a la cuenta corriente del proyecto.

Posibilidades:
  - Cuenta de gastos de proyecto --> cuenta de ingresos de proyecto.
  - Cuenta de gastos de proyecto --> cuenta personal.
  - Cuenta de gastos de proyecto --> cuenta externa.

  - Si hay comisión se genera automáticamente una transferencia entre la cuenta gastos --> Donaciones general de la entidad administradora.
  - Si hay comisión de proyecto se genera automáticamente una una transferencia entre la cuenta gastos --> Donaciones general de la entidad administradora.
  - Si hay reserva de proyecto se genera automáticamente una una transferencia entre la cuenta gastos --> Donaciones corriente del proyecto.
_ _ _


# DEFINICIONES

## Usuario

- Tiene un email, telegram id (temas bots)...
- Se genera un ID para el programa odoo, un id para el backend.
- Tinee un empleado en odoo
- Hay varios tipos de usuario gnb, usuario cooperativo y usuario fakturo.
- Un usuario cooperativo, llevaría el usuario, el empleado y su cuenta personal.
- Un usuario fakturo. Este usuario es equivalente a un usuario cooperativo pero no dispone de sociedad, pero si un acuerdo activista. Puede hacer ingresos, facturas, pago y hoja de gastos. Tiene una comisión extra hacia la entidad administrativa por delegación de responsabilidad.
- Un usuario puede ser registrado en varios grupos.
- Un usuario cooperativo puede hacer ingresos, facturas, pago y hoja de gastos.
- El usuario puede tener varias cuentas personales.
- Un usuario puede ser bloqueado/baneado. (Bloquear es que no puede hacer transferencias).
- Un usuario solo puede estar en un dominio.
- Al crearse un usuario, la cuenta será añadida al dominio por el que haya llegado y quedará baneada hasta que sea validada por el administrador del dominio.


### Puntos técnicos
- crear entidad proyecto (contiene: One2Many usuarios con sus roles en ese proyecto)

--- 

## Definición de Proyecto en GNB 
- Código de proyecto 3 siglas.
* Una cuenta contable de ingresos llamada Cuenta ingresos de proyecto
* Una cuenta contable de gastos llamada Cuenta gastos de proyecto
* Una cuenta contable de reserva llamada Cuenta Corriente de proyecto
* Si contiene sólo un Usuario es un proyecto personal y si son varios es un proyecto Colectivo para GNB, que tendrá:
  * Pueden haber varios usuarios con roles. (Facturador, Lectura y Admin, etc)
  * Tiene un nº impar de firmartes de cuenta para firmar una hoja de gastos.
* Un diario de ingresos (selecciona la cuenta de ingresos)
* Un diario de gastos (selecciona la cuenta de gastos)
* Un producto de proyecto (selecciona la cuenta ingresos y de gastos)
* Configurar porcentaje de reservas.
* El total disponible del proyecto es realmente es el balance entre la cuenta del proyecto de ingresos y gastos

### Dominio
- Un dominio es un el proyecto principal / cooperativa o entidad
- Tiene al menos una cuenta de entrada (paypal,iban...)
- Contiene Grupos
- El dominio puede tener cuenta de interdominio.
- El admin del dominio crea los grupos.
- No puede tener cuenta saldo.

#### Rol Usuario Embajador
- Es un usuario interdominios, siempre tendrá dominio@dominio.
- Tiene un email.

#### Rol Usuario administrador de dominio
- Es el administrador de dominio.
- Tiene un panel de cuentas.
- Tiene un panel de ingresos.
- Tiene un panel de cobros.
- Tiene un panel de gastos.
- Tiene un panel de pagos.
- Tiene un panel de grupos.
- Tiene un panel de usuarios. (Tiene que validar al usuario nuevo).


#### Rol de administrador de cuentas externas.
- Es un usuario del dominio.
- Añade un nuevo ingreso con el tipo de cobro.
- Valida los pagos. (Adjuntar justificante.)

### Cobros
Los ingresos al validarlos se definen como cobros.

### Banca colectiva
- Ingresos y cobros de facturas
- Limites de salida / día / mensual / mensual usuario (recomendado)
- Porcentaje de comisión por transferencias.
- Porcentaje de comisión de proyectos.

_ _ _


### Cuentas
#### Cuenta corriente personal
- Es una cuenta para un usuario
- Tiene un token, que equivale al nº de cuenta HASH 5 creado por el nº de tiempo de creación.
- Es equivalente al saldo de la cuenta personal en la entidad administradora (Cuenta 5150x)
- La cuenta tiene límites de entrada y salida. Apunta warning si es más de 1500. 

#### Cuenta Corriente de proyecto
- Es una cuenta para un proyecto.

- Tiene un token, que equivale al nº de cuenta HASH 5 creado por el nº de tiempo de creación.
- La cuenta tiene límites de entrada y salida.
- La cuenta puede ser multifirma.

#### Cuenta FIAT
- Es una cuenta externa, con valor legal.
- Tiene que permitir entrada y salida.
- Aceptados: Paypal, Stripe, bancarias con IBAN, Cajas en metálico -->
- Tiene al menos un usuario que la administre (rol de administrador de cuentas).

#### Cuenta Caja efectivo
- Es una cuenta de confianza ciega con la persona administradora.
- Tiene que permitir entrada y salida.

#### Cuenta de saldo
- Es una cuenta informativa, queda sin control de este banco pero permite conocer los saldos reales de los grupos.
Ejemplos:
- Cuenta en proveedores.
- Otros cuentas bancarias que no queremos o podemos añadir como Cuenta FIAT.

#### Cuenta interdominios
- Cuenta para permitir traspasos entre los mismos.
- Deben estar en positivo para poder usarse.
- Hay que crear un usuario embajador, que permite leer el saldo de la cuenta interdominio y viceversa.

_ _ _

#### Ejemplo saldo vista usuario
1000 - 800 - 100 - 100 (Reserva de proyecto)

#### Salida de dinero / tranferencia
- Configurar y personalizar reglas para efectuar salidas, ejemplo. Crear hojas de gastos para justificar con viajes, dietas y pernoctas. (Justificación suficiente para asociaciones y cooperativas en el ámbito de voluntariado. Más consultas abogados

## Más Ideas
- Pasar a cuenta segura
- Comprar o intercambiar monedas

Llega el ingreso se apunta en la cuenta que llega (Notas de donde viene - Concepto)
Cuenta Cooperativa Banco 1 (IBAN)
Cuenta Cooperativa Banco 2 (IBAN)

Se trasfiere a la cuenta del usuario el máx disponible - Y el correspondiente reserva si tiene automatico

## TODO Facturas
    Como detectar que el PDF está disponible.
    Como generar el PDF via API sin entrar a ODOO (solo admins del proyecto o superadmins)
    Avisar al usuario que su factura ya está descargable... (sin adjuntarla en el email)
    Permitir descargar el pdf desde el gnb
    Permitir usar plantillas propias por cada proyecto para generar PDF... (+ adelante)

## Facturas
- Poner un cron en el django las facturas de draft a posted y eso crea un accion de aviso email/notif al factuador.
- Las plantillas .html / generaran el PDF con un enlace django.

## Testings
- Usar Selenium Wire o Python Pyppeteer
- Transformar código de tests BDD de Gherkin a Selenium usando Cucumber
